package io.renren.modules.estateAgency.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author limingle
 * @version 1.0.0
 * @ClassName AbstractController.java
 * @Description Controller公共组件
 * @createTime 2020/9/30 13:40
 */
public abstract class AbstractController {
    protected Logger logger = LoggerFactory.getLogger(getClass());
}
