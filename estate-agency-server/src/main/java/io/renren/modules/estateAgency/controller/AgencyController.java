package io.renren.modules.estateAgency.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.estateAgency.entity.AgencyEntity;
import io.renren.modules.estateAgency.service.AgencyService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@RestController
@RequestMapping("estateAgency/agency")
public class AgencyController {
    @Autowired
    private AgencyService agencyService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("estateAgency:agency:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = agencyService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 列表All
     */
    @RequestMapping("/listAll")
    @RequiresPermissions("estateAgency:agency:list")
    public R listAll(@RequestParam Map<String, Object> params) {
        List<AgencyEntity> list = agencyService.list();

        return R.ok().put("data", list);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("estateAgency:agency:info")
    public R info(@PathVariable("id") Integer id) {
        AgencyEntity agency = agencyService.getById(id);

        return R.ok().put("data", agency);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("estateAgency:agency:save")
    public R save(@RequestBody AgencyEntity agency) {
        ValidatorUtils.validateEntity(agency);
        if (agency.getId() == null) {
            agencyService.save(agency);
        } else {
            agencyService.updateById(agency);
        }

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("estateAgency:agency:delete")
    public R delete(@RequestBody Integer[] ids) {
        agencyService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
