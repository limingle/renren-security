package io.renren.modules.estateAgency.controller;

import io.renren.common.utils.Constant;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.estateAgency.entity.AreaCategoryEntity;
import io.renren.modules.estateAgency.service.AreaCategoryService;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;



/**
 * 小区分类管理
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-03-27 16:54:03
 */
@Api(value = "小区分类管理", tags = "小区分类管理")
@RestController
@RequestMapping("estateAgency/areacategory")
public class AreaCategoryController extends AbstractController {
    @Autowired
    private AreaCategoryService areaCategoryService;

    /**
     * 列表
     */
    @ApiOperation(value = "小区类别查询-列表", notes = "小区类别查询-列表")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @GetMapping("/list")
    @RequiresPermissions("estateAgency:areacategory:list")
    public R list(){
        List<AreaCategoryEntity> categoryList=areaCategoryService.queryList(new HashMap<String,Object>());
        return R.ok().put("categoryList", categoryList);
    }

    /**
     * 树形结构
     */
    @ApiOperation(value = "小区类别查询-树", notes = "小区类别查询-树")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @GetMapping("/tree")
    @RequiresPermissions("estateAgency:areacategory:list")
    public R tree(){
        List<AreaCategoryEntity> categoryList=areaCategoryService.queryTree(new HashMap<String,Object>());
        return R.ok().put("categoryList", categoryList);
    }

    /**
     * 选择小区类别(添加、修改菜单)
     */
    @ApiOperation(value = "选择小区类别", notes = "选择小区类别")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @GetMapping("/select")
    @RequiresPermissions("estateAgency:areacategory:select")
    public R select(){
        List<AreaCategoryEntity> categoryList = areaCategoryService.queryList(new HashMap<String, Object>());

        //添加一级部门
        if(getUserId() == Constant.SUPER_ADMIN){
            AreaCategoryEntity root = new AreaCategoryEntity();
            root.setCategoryId(0L);
            root.setName("一级类别");
            root.setParentId(-1L);
            root.setOpen(true);
            categoryList.add(root);
        }

        return R.ok().put("categoryList", categoryList);
    }

    /**
     * 上级类别Id(管理员则为0)
     */
    @ApiOperation(value = "查询上级类别ID", notes = "查询上级类别ID")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @GetMapping("/info")
    @RequiresPermissions("estateAgency:areacategory:list")
    public R info(){
        long categoryId = 0;
        if(getUserId() != Constant.SUPER_ADMIN){
            List<AreaCategoryEntity> categoryList = areaCategoryService.queryList(new HashMap<String, Object>());
            Long parentId = null;
            for(AreaCategoryEntity categoryEntity : categoryList){
                if(parentId == null){
                    parentId = categoryEntity.getParentId();
                    continue;
                }

                if(parentId > categoryEntity.getParentId().longValue()){
                    parentId = categoryEntity.getParentId();
                }
            }
            categoryId = parentId;
        }

        return R.ok().put("categoroyId", categoryId);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "获取小区类别信息", notes = "获取小区类别信息")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @GetMapping("/info/{cateoryId}")
    @RequiresPermissions("estateAgency:areacategory:info")
    public R info(@PathVariable("cateoryId") Long cateoryId){
        AreaCategoryEntity areaCategory = areaCategoryService.getById(cateoryId);

        return R.ok().put("areaCategory", areaCategory);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存小区类别信息", notes = "保存小区类别信息")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/save")
    @RequiresPermissions("estateAgency:areacategory:save")
    public R save(@ApiParam(value = "小区类别信息",required = true) @RequestBody AreaCategoryEntity areaCategory){
        areaCategoryService.save(areaCategory);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "保存小区类别信息", notes = "保存小区类别信息")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/update")
    @RequiresPermissions("estateAgency:areacategory:update")
    public R update(@ApiParam(value = "小区类别信息",required = true) @RequestBody AreaCategoryEntity areaCategory){
        ValidatorUtils.validateEntity(areaCategory);
        areaCategoryService.updateById(areaCategory);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除小区类别信息", notes = "删除小区类别信息")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/delete")
    @RequiresPermissions("estateAgency:areacategory:delete")
    public R delete(long categoryId){
        //判断是否有子部门
        List<Long> categoryList = areaCategoryService.queryCategoryIdList(categoryId);
        if(categoryList.size() > 0){
            return R.error("请先删除子类别");
        }

        areaCategoryService.removeById(categoryId   );

        return R.ok();
    }

}
