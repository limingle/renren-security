package io.renren.modules.estateAgency.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.renren.common.annotation.SysLog;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.estateAgency.dto.AreaInfoSearchParamDTO;
import io.renren.modules.estateAgency.entity.AreaInfoEntity;
import io.renren.modules.estateAgency.enums.FileType;
import io.renren.modules.estateAgency.service.AreaCategoryService;
import io.renren.modules.estateAgency.service.AreaInfoService;
import io.renren.modules.estateAgency.service.BusinessFileService;
import io.renren.modules.estateAgency.vo.AreaInfoDetailVO;
import io.renren.modules.estateAgency.vo.AreaInfoSearchListVO;
import io.renren.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * 小区信息
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 14:35:57
 */
@RestController
@RequestMapping("estateAgency/areainfo")
@Api(value = "小区管理", tags = "小区管理")
public class AreaInfoController extends AbstractController {
    @Autowired
    private AreaInfoService areaInfoService;
    @Autowired
    private AreaCategoryService areaCategoryService;
    private List<AreaInfoSearchListVO> list;

    @Autowired
    private BusinessFileService businessFileService;

    /**
     * @throws
     * @title queryAreaInfoList
     * @description 查询小区列表
     * @author limingle
     * @param: paramDTO
     * @updateTime 2020/10/20 16:34
     * @return: io.renren.common.utils.R
     */
    @SysLog("查询小区列表")
    @ApiOperation(value = "小区查询", notes = "小区查询")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/queryAreaInfoList")
    @RequiresPermissions("estateAgency:areainfo:list")
    public R queryAreaInfoList(@ApiParam(value = "查询参数", required = true) @RequestBody AreaInfoSearchParamDTO paramDTO) {
        List<AreaInfoSearchListVO> list = null;
        try {
            if (!checkParam(paramDTO)) {
                return R.error("查询参数不正确！");
            }
            PageHelper.startPage(paramDTO.getPageNum(), paramDTO.getPageSize());
            list = areaInfoService.queryAreaInfoSearchList(paramDTO);

            for (AreaInfoSearchListVO item : list) {
                List<Long> fileIds = businessFileService.queryFileIdsBybusinessId(item.getId(), FileType.AREA_FILE.getCode());
                item.setAreaCategoryStringArr(areaCategoryService.getAreaCategoryParent(item.getAreaCategory()));
                item.setFileIds(fileIds);
            }
        } catch (Exception e) {
            logger.error("查询数据异常:", e);
            return R.error("查询数据异常!");
        }

        return R.ok().put("data", new PageInfo<>(list));
    }

    /**
     * @throws
     * @title 参数校验
     * @description
     * @author limingle
     * @updateTime 2020/10/20 16:33
     */
    private boolean checkParam(AreaInfoSearchParamDTO paramDTO) {
        boolean flag = true;
        if (null == paramDTO.getPageNum() || null == paramDTO.getPageSize()) {
            flag = false;
        }
        return flag;
    }


    /**
     * 信息
     */
    @ApiOperation(value = "小区详情", notes = "小区详情")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @GetMapping("/info/{id}")
    @RequiresPermissions("estateAgency:areainfo:info")
    public R info(@ApiParam(value = "小区ID", required = true) @PathVariable("id") Long id) {
        AreaInfoDetailVO detail = areaInfoService.getAreaInfoDetail(id);
        List<Long> fileIds = businessFileService.queryFileIdsBybusinessId(detail.getId(), FileType.AREA_FILE.getCode());
        detail.setAreaCategoryStringArr(areaCategoryService.getAreaCategoryParent(detail.getAreaCategory()));
        detail.setAreaCategoryIdArr(areaCategoryService.getAreaCategoryParentId(detail.getAreaCategory()));
        detail.setFileIds(fileIds);

        return R.ok().put("data", detail);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存小区", notes = "保存小区")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/save")
    @RequiresPermissions("estateAgency:areainfo:save")
    public R save(@ApiParam(value = "小区信息", required = true) @RequestBody AreaInfoEntity areaInfo) {
        ValidatorUtils.validateEntity((areaInfo));
        if (areaInfo.getId() == null) {
            Long userId = ((SysUserEntity) SecurityUtils.getSubject().getPrincipal()).getUserId();
            areaInfo.setCreateUser(userId);
            areaInfo.setCreateTime(new Date());
            areaInfoService.saveAreaInfo(areaInfo);
        } else {
            Long userId = ((SysUserEntity) SecurityUtils.getSubject().getPrincipal()).getUserId();
            areaInfo.setUpdateUser(userId);
            areaInfo.setUpdateTime(new Date());
            areaInfoService.updateAreaInfo(areaInfo);
        }

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除小区", notes = "删除小区")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/delete")
    @RequiresPermissions("estateAgency:areainfo:delete")
    public R delete(@ApiParam(value = "小区Id数组", required = true) @RequestBody Long[] ids) {
        areaInfoService.removeAreaByIds(Arrays.asList(ids));

        return R.ok();
    }

}
