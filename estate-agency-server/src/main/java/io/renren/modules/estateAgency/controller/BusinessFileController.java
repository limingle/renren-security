package io.renren.modules.estateAgency.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.estateAgency.entity.BusinessFileEntity;
import io.renren.modules.estateAgency.service.BusinessFileService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@RestController
@RequestMapping("estateAgency/businessfile")
public class BusinessFileController {
    @Autowired
    private BusinessFileService businessFileService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("estateAgency:businessfile:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = businessFileService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("estateAgency:businessfile:info")
    public R info(@PathVariable("id") Long id){
        BusinessFileEntity businessFile = businessFileService.getById(id);

        return R.ok().put("businessFile", businessFile);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("estateAgency:businessfile:save")
    public R save(@RequestBody BusinessFileEntity businessFile){
        businessFileService.save(businessFile);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("estateAgency:businessfile:update")
    public R update(@RequestBody BusinessFileEntity businessFile){
        ValidatorUtils.validateEntity(businessFile);
        businessFileService.updateById(businessFile);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("estateAgency:businessfile:delete")
    public R delete(@RequestBody Long[] ids){
        businessFileService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
