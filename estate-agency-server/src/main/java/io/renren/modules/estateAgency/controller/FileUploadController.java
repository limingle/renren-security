package io.renren.modules.estateAgency.controller;

import com.alibaba.fastjson.JSON;
import io.renren.common.utils.R;
import io.renren.modules.estateAgency.entity.FileUpload;
import io.renren.modules.estateAgency.service.FileUploadService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.shiro.ShiroUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/file-upload")
public class FileUploadController {

    private static Logger logger = LoggerFactory.getLogger(FileUploadController.class);

    @Autowired
    private FileUploadService fileUploadService;

    @RequestMapping(value = "/upload", produces = {"application/json"}, method = RequestMethod.POST)
    public R uploadFile(@RequestParam(value = "uploadFile", required = true) MultipartFile uploadFile, HttpServletRequest request) {
//        UserInfo userInfo = (UserInfo) request.getAttribute("userInfo");
        SysUserEntity user = ShiroUtils.getUserEntity();
        MultipartFile file = uploadFile;
        String fileName = file.getOriginalFilename();
        //用户ID
        Long userId = user.getUserId();
        //文件后缀名
        String extension = FilenameUtils.getExtension(fileName);
        String fileId = UUID.randomUUID().toString();
        //保存到系统的文件名
        String saveFileName = fileId + "." + extension;
        //获取文件大小
        Long size = file.getSize();

        FileUpload fileUpload = new FileUpload();
//        fileUpload.setFileId(fileId);
        fileUpload.setOptUser(userId);
        fileUpload.setFileName(fileName);
        fileUpload.setFileSize(size);
        fileUpload.setFormat(extension);
//        fileUpload.setOptUser(userInfo.getUserId());
        fileUpload.setCreateDate(new Date());

        try {
            Boolean flag = fileUploadService.saveFile(file.getInputStream(), file.getSize(), userId.toString(), saveFileName, fileUpload);
            if (flag) {
                return R.ok().put("data", fileUpload);
            } else {
                return R.error("上传文件到服务器出现问题!");
            }
        } catch (IOException e) {
            logger.error("FileUploadController uploadFile方法 出现异常!", e);
            return R.error("上传文件到服务器出现问题!");
        }
    }


    @RequestMapping(value = "/download")
    public void downloadFile(String fileId, Integer isView, HttpServletRequest request, HttpServletResponse response) {
        try {
            logger.info("downloadFile fileId:{}", JSON.toJSONString(fileId));
            if (!StringUtils.isEmpty(fileId)) {
                HashMap<String, Object> downloadFile = fileUploadService.downloadFile(fileId);
                InputStream inputStream = (InputStream) downloadFile.get("in");
                String fileLable = (String) downloadFile.get("fileName");

                // 设置响应头，控制浏览器下载该文件
                String cdType = "attachment";
                String contentType = "application/octet-stream;charset=utf-8";
                if (isView != null && isView == 1) {
                    cdType = "inline";
                    contentType = "image/*";
                }
                response.reset();
                response.setContentType(contentType);
//                response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileLable, "UTF-8"));
                if (isView == null) {
                    response.setHeader("Content-Disposition", cdType + ";filename=" + new String(fileLable.getBytes("UTF-8"), "ISO-8859-1"));
                }
                response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");

                OutputStream os = response.getOutputStream();
                byte[] b = new byte[8192];
                int length;
                while ((length = inputStream.read(b)) > 0) {
                    os.write(b, 0, length);
                }
                os.close();
                inputStream.close();
            } else {
                logger.info("downloadFile fileId在数据库中查不到记录!");
            }
        } catch (Exception e) {
            logger.error("FileUploadController downloadFile方法 出现异常!", e);
        }
    }

    @GetMapping(value = "/{ids}", produces = {"application/json"})
    @ApiOperation(value = "查询", notes = "查询", response = R.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "查询成功", response = R.class)})
    public R findFileUpload(@PathVariable("ids") List<String> ids, HttpServletRequest request) {
        logger.info("查询上传文件-findFileUpload ids:{}", JSON.toJSONString(ids));
        List<FileUpload> result = fileUploadService.fileUploadSearch(ids);
        return R.ok().put("data", result);
    }

}
