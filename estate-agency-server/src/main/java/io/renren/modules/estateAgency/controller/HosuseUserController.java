package io.renren.modules.estateAgency.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.estateAgency.entity.HosuseUserEntity;
import io.renren.modules.estateAgency.service.HosuseUserService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@RestController
@RequestMapping("estateAgency/hosuseuser")
public class HosuseUserController {
    @Autowired
    private HosuseUserService hosuseUserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("estateAgency:hosuseuser:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = hosuseUserService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("estateAgency:hosuseuser:info")
    public R info(@PathVariable("id") Long id){
        HosuseUserEntity hosuseUser = hosuseUserService.getById(id);

        return R.ok().put("hosuseUser", hosuseUser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("estateAgency:hosuseuser:save")
    public R save(@RequestBody HosuseUserEntity hosuseUser){
        hosuseUserService.save(hosuseUser);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("estateAgency:hosuseuser:update")
    public R update(@RequestBody HosuseUserEntity hosuseUser){
        ValidatorUtils.validateEntity(hosuseUser);
        hosuseUserService.updateById(hosuseUser);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("estateAgency:hosuseuser:delete")
    public R delete(@RequestBody Long[] ids){
        hosuseUserService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
