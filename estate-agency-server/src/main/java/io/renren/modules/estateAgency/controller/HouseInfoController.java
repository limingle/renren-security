package io.renren.modules.estateAgency.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.estateAgency.entity.HouseInfoEntity;
import io.renren.modules.estateAgency.service.HouseInfoService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@RestController
@RequestMapping("estateAgency/houseinfo")
public class HouseInfoController {
    @Autowired
    private HouseInfoService houseInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("estateAgency:houseinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = houseInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("estateAgency:houseinfo:info")
    public R info(@PathVariable("id") Long id){
        HouseInfoEntity houseInfo = houseInfoService.getById(id);

        return R.ok().put("houseInfo", houseInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("estateAgency:houseinfo:save")
    public R save(@RequestBody HouseInfoEntity houseInfo){
        houseInfoService.save(houseInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("estateAgency:houseinfo:update")
    public R update(@RequestBody HouseInfoEntity houseInfo){
        ValidatorUtils.validateEntity(houseInfo);
        houseInfoService.updateById(houseInfo);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("estateAgency:houseinfo:delete")
    public R delete(@RequestBody Long[] ids){
        houseInfoService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
