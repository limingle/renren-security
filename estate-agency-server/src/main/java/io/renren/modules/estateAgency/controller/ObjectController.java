package io.renren.modules.estateAgency.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.estateAgency.entity.ObjectEntity;
import io.renren.modules.estateAgency.service.ObjectService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
@RestController
@RequestMapping("estateAgency/object")
@Api(value = "对象管理", tags = "对象管理")
public class ObjectController {
    @Autowired
    private ObjectService objectService;

    /**
     * @title list
     * @description
     * @author limingle
     * @updateTime 2021/1/12 9:44
     * @throws
     */
    @RequestMapping("/list")
    @RequiresPermissions("estateAgency:object:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = objectService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("estateAgency:object:info")
    public R info(@PathVariable("id") Long id){
        ObjectEntity object = objectService.getById(id);

        return R.ok().put("object", object);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("estateAgency:object:save")
    public R save(@RequestBody ObjectEntity object){
        objectService.save(object);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("estateAgency:object:update")
    public R update(@RequestBody ObjectEntity object){
        ValidatorUtils.validateEntity(object);
        objectService.updateById(object);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("estateAgency:object:delete")
    public R delete(@RequestBody Long[] ids){
        objectService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
