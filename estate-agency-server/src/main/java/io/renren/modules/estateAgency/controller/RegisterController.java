package io.renren.modules.estateAgency.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.renren.common.annotation.SysLog;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.estateAgency.dto.RegisterSearchParamDTO;
import io.renren.modules.estateAgency.entity.BusinessFileEntity;
import io.renren.modules.estateAgency.entity.RegisterEntity;
import io.renren.modules.estateAgency.service.BusinessFileService;
import io.renren.modules.estateAgency.service.RegisterService;
import io.renren.modules.estateAgency.vo.RegisterSearchListVO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@RestController
@RequestMapping("estateAgency/register")
public class RegisterController extends AbstractController {
    @Autowired
    private RegisterService registerService;

    @Autowired
    private BusinessFileService businessFileService;

    /**
     * @title queryRegisterList
     * @description 查询经纪人列表
     * @author limingle
     * @param: paramDTO
     * @updateTime 2020/9/30 13:45
     * @return: io.renren.common.utils.R
     * @throws
     */
    @SysLog("查询经纪人列表")
    @PostMapping("/queryRegisterList")
    @RequiresPermissions("estateAgency:register:list")
    public R queryRegisterList(@RequestBody RegisterSearchParamDTO paramDTO) {
        List<RegisterSearchListVO> list = null;
        try {
            if (!checkParam(paramDTO)) {
                return R.error("查询参数不正确！");
            }
            PageHelper.startPage(paramDTO.getPageNum(), paramDTO.getPageSize());
            list = registerService.queryRegisterList(paramDTO);
        } catch (Exception e) {
            logger.error("查询数据异常:", e);
            return R.error("查询数据异常!");
        }

        return R.ok().put("data", new PageInfo<>(list));
    }


    /**
     * 参数校验
     *
     * @param paramDTO
     * @author: limingle
     * @date: 2020/2/20 18:03
     */
    private boolean checkParam(RegisterSearchParamDTO paramDTO) {
        boolean flag = true;
        if (null == paramDTO.getPageNum() || null == paramDTO.getPageSize()) {
            flag = false;
        }
        return flag;
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("estateAgency:register:info")
    public R info(@PathVariable("id") Long id) {
        RegisterEntity register = registerService.getById(id);
        QueryWrapper<BusinessFileEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", register.getId());
        BusinessFileEntity businessFile = businessFileService.getOne(queryWrapper);
        if (businessFile != null) {
            register.setFileId(businessFile.getFileId());
        }
        DozerBeanMapper mapper = new DozerBeanMapper();
        RegisterSearchListVO registerSearchListVO = mapper.map(register,RegisterSearchListVO.class);

        return R.ok().put("data", registerSearchListVO);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("estateAgency:register:save")
    public R update(@RequestBody RegisterEntity register) {
        ValidatorUtils.validateEntity(register);
        if (register.getId() == null) {
            register.setType(2);
            register.setCreateTime(new Date());
            registerService.saveRegister(register);
        } else {
            registerService.update(register);
        }

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("estateAgency:register:delete")
    public R delete(@RequestBody Long[] ids) {
        registerService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
