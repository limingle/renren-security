package io.renren.modules.estateAgency.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.estateAgency.entity.RentEntity;
import io.renren.modules.estateAgency.service.RentService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
@RestController
@RequestMapping("estateAgency/rent")
public class RentController {
    @Autowired
    private RentService rentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("estateAgency:rent:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = rentService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("estateAgency:rent:info")
    public R info(@PathVariable("id") Long id){
        RentEntity rent = rentService.getById(id);

        return R.ok().put("rent", rent);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("estateAgency:rent:save")
    public R save(@RequestBody RentEntity rent){
        rentService.save(rent);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("estateAgency:rent:update")
    public R update(@RequestBody RentEntity rent){
        ValidatorUtils.validateEntity(rent);
        rentService.updateById(rent);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("estateAgency:rent:delete")
    public R delete(@RequestBody Long[] ids){
        rentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
