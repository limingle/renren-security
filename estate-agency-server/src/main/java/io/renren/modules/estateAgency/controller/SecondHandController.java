package io.renren.modules.estateAgency.controller;

import com.github.pagehelper.PageInfo;
import io.renren.common.annotation.SysLog;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.estateAgency.dto.SecondHandSaveDTO;
import io.renren.modules.estateAgency.dto.SecondHandSearchParamDTO;
import io.renren.modules.estateAgency.service.ObjectService;
import io.renren.modules.estateAgency.service.SecondHandService;
import io.renren.modules.estateAgency.vo.SecondHandDetailVO;
import io.renren.modules.estateAgency.vo.SecondHandSearchListVO;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
@RestController
@RequestMapping("estateAgency/secondhand")
@Api(value = "二手房管理", tags = "二手房管理")
public class SecondHandController extends AbstractController {
    @Autowired
    private SecondHandService secondHandService;
    @Autowired
    private ObjectService objectService;

    /**
     * @title 二手房查询列表
     * @description
     * @author limingle
     * @updateTime 2021/1/13 16:05
     * @throws
     * @return: io.renren.common.utils.R
     */
    @SysLog("查询二手房列表")
    @ApiOperation(value = "二手房查询", notes = "二手房查询")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/list")
    @RequiresPermissions("estateAgency:secondhand:list")
    public R list(@ApiParam(value = "查询参数", required = true) @RequestBody SecondHandSearchParamDTO paramDTO){
        List<SecondHandSearchListVO> list=null;
        try {
            if(!checkParam(paramDTO)){
                return R.error("查询参数不正确！");
            }
            list=secondHandService.querySecondHandSearchList(paramDTO);
        }catch (Exception e){
            logger.error("查询数据异常:", e);
            return R.error("查询数据异常!");
        }

        return R.ok().put("data", new PageInfo<>(list));
    }

    /**
     * @title 参数校验
     * @description
     * @author limingle
     * @updateTime 2021/1/13 18:58
     * @throws
     */
    private boolean checkParam(SecondHandSearchParamDTO paramDTO) {
        boolean flag = true;
        if (null == paramDTO.getPageNum() || null == paramDTO.getPageSize()) {
            flag = false;
        }
        return flag;
    }


    /**
     * 信息
     */
    @ApiOperation(value = "二手房详情", notes = "二手房详情")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @GetMapping("/info/{id}")
    @RequiresPermissions("estateAgency:secondhand:info")
    public R info(@PathVariable("id") Long id){
        SecondHandDetailVO secondHand = secondHandService.getSecondHandById(id);

        return R.ok().put("data", secondHand);
    }

    /**
     * @title 保存二手房
     * @description
     * @author limingle
     * @updateTime 2021/1/15 9:56
     * @throws
     */
    @ApiOperation(value = "保存二手房", notes = "保存二手房")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/save")
    @RequiresPermissions("estateAgency:secondhand:save")
    public R save(@ApiParam(value = "二手房信息", required = true)@RequestBody SecondHandSaveDTO secondHand){
        ValidatorUtils.validateEntity(secondHand);
        secondHandService.saveSecondHand(secondHand);

        return R.ok();
    }

    /**
     * @title 删除二手房
     * @description
     * @author limingle
     * @updateTime 2021/1/15 10:58
     * @throws
     */
    @ApiOperation(value = "删除二手房", notes = "删除二手房")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/delete")
    @RequiresPermissions("estateAgency:secondhand:delete")
    public R delete(@ApiParam(value = "二手房Id数组", required = true)@RequestBody Long[] ids){
        secondHandService.removeSecondHandByIds(Arrays.asList(ids));

        return R.ok();
    }

}
