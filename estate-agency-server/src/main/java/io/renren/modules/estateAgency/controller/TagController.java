package io.renren.modules.estateAgency.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.estateAgency.entity.TagEntity;
import io.renren.modules.estateAgency.service.TagService;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 标签管理
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-05-10 14:45:21
 */
@Api(value = "标签管理", tags = "标签管理")
@RestController
@RequestMapping("estateAgency/tag")
public class TagController extends AbstractController {
    @Autowired
    private TagService tagService;

    /**
     * 列表
     */
    @ApiOperation(value = "标签查询-列表", notes = "标签查询-列表")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @GetMapping("/list")
    @RequiresPermissions("estateAgency:tag:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = tagService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 树形结构
     */
    @ApiOperation(value = "标签查询-树", notes = "标签查询-树")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @GetMapping("/tree")
    @RequiresPermissions("estateAgency:tag:list")
    public R tree() {
        List<TagEntity> tagList = tagService.queryTree();
        return R.ok().put("tagList", tagList);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{tagId}")
    @RequiresPermissions("estateAgency:tag:info")
    public R info(@PathVariable("tagId") Long tagId) {
        TagEntity tag = tagService.getById(tagId);

        return R.ok().put("tag", tag);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存标签信息", notes = "保存标签信息")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/save")
    @RequiresPermissions("estateAgency:tag:save")
    public R save(@ApiParam(value = "小区类别信息",required = true) @RequestBody TagEntity tag) {
        tagService.save(tag);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "更新标签信息", notes = "更新标签信息")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/update")
    @RequiresPermissions("estateAgency:tag:update")
    public R update(@ApiParam(value = "小区类别信息",required = true) @RequestBody TagEntity tag) {
        ValidatorUtils.validateEntity(tag);
        tagService.updateById(tag);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除标签信息", notes = "删除标签信息")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "返回结果")})
    @PostMapping("/delete")
    @RequiresPermissions("estateAgency:tag:delete")
    public R delete(@ApiParam(value = "标签ID",required = true) @RequestParam Long tagId) {
        //判断是否有子部门
//        List<Long> tagIdList = tagService.queryTagIdList(tagId);
//        if(tagIdList.size() > 0){
//            return R.error("请先删除子类别");
//        }

        tagService.deleteTag(tagId);

        return R.ok();
    }

}
