package io.renren.modules.estateAgency.dao;

import io.renren.modules.estateAgency.entity.AgencyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@Mapper
public interface AgencyDao extends BaseMapper<AgencyEntity> {
	
}
