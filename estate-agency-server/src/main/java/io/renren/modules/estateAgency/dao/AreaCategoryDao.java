package io.renren.modules.estateAgency.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.estateAgency.entity.AreaCategoryEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 小区分类管理
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-03-27 16:54:03
 */
@Mapper
public interface AreaCategoryDao extends BaseMapper<AreaCategoryEntity> {
    List<AreaCategoryEntity> queryList(Map<String, Object> params);

    /**
     * 查询子部门ID列表
     * @param parentId  上级部门ID
     */
    List<Long> queryCategoryIdList(Long parentId);

    List<Map<String,Object>> getAreaCategoryParentId(Long categoryId);

    List<Map<String,Object>> getAreaCategoryParent(Long categoryId);
}
