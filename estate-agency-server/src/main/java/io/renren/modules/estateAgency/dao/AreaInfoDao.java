package io.renren.modules.estateAgency.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.estateAgency.dto.AreaInfoSearchParamDTO;
import io.renren.modules.estateAgency.entity.AreaInfoEntity;
import io.renren.modules.estateAgency.vo.AreaInfoDetailVO;
import io.renren.modules.estateAgency.vo.AreaInfoSearchListVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 14:35:57
 */
@Mapper
public interface AreaInfoDao extends BaseMapper<AreaInfoEntity> {

    /**
     * @title 根据条件查询区域信息
     * @description
     * @author limingle
     * @updateTime 2020/10/20 10:09
     * @throws
     */
    List<AreaInfoSearchListVO> queryAreaInfoSearchList(AreaInfoSearchParamDTO paramDTO);

    AreaInfoDetailVO getAreaInfoDetail(Long id);

    void removeAreaByIds(@Param("asList") List<Long> asList);
}
