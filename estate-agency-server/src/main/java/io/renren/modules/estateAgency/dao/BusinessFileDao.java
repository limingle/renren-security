package io.renren.modules.estateAgency.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.estateAgency.entity.BusinessFileEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@Mapper
public interface BusinessFileDao extends BaseMapper<BusinessFileEntity> {

    List<Long> queryFileIdsBybusinessId(@Param("business_id") Long businessId, @Param("type") Integer type);
}
