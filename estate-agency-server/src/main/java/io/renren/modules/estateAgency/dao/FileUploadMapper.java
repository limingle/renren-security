package io.renren.modules.estateAgency.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.estateAgency.entity.FileUpload;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface FileUploadMapper extends BaseMapper<FileUpload> {
}

