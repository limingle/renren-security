package io.renren.modules.estateAgency.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.estateAgency.dto.RegisterSearchParamDTO;
import io.renren.modules.estateAgency.entity.RegisterEntity;
import io.renren.modules.estateAgency.vo.RegisterSearchListVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@Mapper
public interface RegisterDao extends BaseMapper<RegisterEntity> {
    /**
     * 查询经纪人信息
     * @return
     */
    List<RegisterEntity> queryList();

    List<RegisterSearchListVO> queryRegisterList(RegisterSearchParamDTO paramDTO);
}
