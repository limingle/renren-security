package io.renren.modules.estateAgency.dao;

import io.renren.modules.estateAgency.entity.RentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
@Mapper
public interface RentDao extends BaseMapper<RentEntity> {
	
}
