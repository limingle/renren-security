package io.renren.modules.estateAgency.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.estateAgency.dto.SecondHandSearchListDTO;
import io.renren.modules.estateAgency.dto.SecondHandSearchParamDTO;
import io.renren.modules.estateAgency.entity.SecondHandEntity;
import io.renren.modules.estateAgency.vo.SecondHandDetailVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
@Mapper
public interface SecondHandDao extends BaseMapper<SecondHandEntity> {

    List<SecondHandSearchListDTO> querySecondHandSearchList(SecondHandSearchParamDTO paramDTO);

    SecondHandDetailVO querySecondHandDetail(@Param("id") Long id);
}
