package io.renren.modules.estateAgency.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.estateAgency.entity.TagEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 标签管理
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-05-10 14:45:21
 */
@Mapper
public interface TagDao extends BaseMapper<TagEntity> {
    List<TagEntity> queryList(Map<String, Object> params);
    List<Long> queryTagIdList(Long parentId);

}
