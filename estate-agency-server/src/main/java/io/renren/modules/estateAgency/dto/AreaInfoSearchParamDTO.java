package io.renren.modules.estateAgency.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author limingle
 * @version 1.0.0
 * @ClassName AreaInfoSearchParamDTO.java
 * @Description TODO
 * @createTime 2020/10/20 10:07
 */
@Data
public class AreaInfoSearchParamDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 当前页数
     */
    private Integer pageNum;
    /**
     * 每页显示条数
     */
    private Integer pageSize;
    /**
     * 主标题
     */
    private String mainTitle;
}
