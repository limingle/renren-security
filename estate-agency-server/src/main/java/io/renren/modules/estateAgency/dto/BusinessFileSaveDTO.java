package io.renren.modules.estateAgency.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author limingle
 * @version 1.0.0
 * @ClassName SecondHandSearchListDTO.java
 * @Description TODO
 * @createTime 2021/1/13 17:43
 */
@Data
public class BusinessFileSaveDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务数据主键
     */
    private Long businessId;
    /**
     * 文件主键
     */
    private Long fileId;
    /**
     * 排序
     */
    private Integer sequence;
    /**
     * 业务类型 0:小区图片 1:房屋图片 2:房本材料 3:人员头像 11:室 12:厅 13:厨 14:卫 15:阳台 16:户型图 17:室外图
     */
    private Integer type;
}
