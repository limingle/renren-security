package io.renren.modules.estateAgency.dto;

import lombok.Data;

import java.io.Serializable;


/**
 * @author limingle
 * @version 1.0.0
 * @ClassName RegisterSearchParamDTO.java
 * @Description 经纪人查询参数DTO
 * @createTime 2020/9/30 13:23
 */
@Data
public class RegisterSearchParamDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 当前页数
     */
    private Integer pageNum;
    /**
     * 每页显示条数
     */
    private Integer pageSize;
}
