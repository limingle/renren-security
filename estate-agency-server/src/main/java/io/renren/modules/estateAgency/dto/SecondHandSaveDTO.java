package io.renren.modules.estateAgency.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author limingle
 * @version 1.0.0
 * @ClassName SecondHandSearchListDTO.java
 * @Description TODO
 * @createTime 2021/1/13 17:43
 */
@Data
public class SecondHandSaveDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 房屋主键
     */
    private Long id;
    /**
     * 房屋类型 1:销售，2:出租
     */
    private Integer type;
    /**
     * 小区id
     */
    private Long communityId;
    /**
     * 栋/幢/弄/胡同
     */
    private Integer houseNumberBuilding;
    /**
     * 单元/号
     */
    private Integer houseNumberUnit;
    /**
     * 门牌
     */
    private Integer houseNumberRoom;
    /**
     * 楼层（当前）
     */
    private Long currentFloor;
    /**
     * 楼层（共）
     */
    private Long totalFloor;
    /**
     * 实际楼层
     */
    private Long actualFloor;
    /**
     * 户型（室）
     */
    private Integer room;
    /**
     * 户型（厅）
     */
    private Integer hall;
    /**
     * 户型（卫）
     */
    private Integer bathroom;
    /**
     * 建筑面积
     */
    private Double constructionArea;
    /**
     * 套内面积
     */
    private Double innerBuildingArea;
    /**
     * 标题
     */
    private String smartTitle;
    /**
     * 主卧朝向
     */
    private String bedroomOriented;
    /**
     * 房屋类型
     */
    private String houseTypes;
    /**
     * 装修情况
     */
    private String decorationStatus;
    /**
     * 是否有电梯 0：没有  1：有
     */
    private Integer elevator;
    /**
     * 供暖
     */
    private String heating;
    /**
     * 房源详情
     */
    private String houseDetail;
    /**
     * 售价
     */
    private Double price;
    /**
     * 每层住户数量
     */
    private Integer floorRoomCount;
    /**
     * 房主
     */
    private String homeowner;
    /**
     * 房主电话
     */
    private String homeownerPhone;
    /**
     * 房主电话备用
     */
    private String homeownerPhone1;
    /**
     * 房主微信
     */
    private String homeownerWx;
    /**
     * 服务介绍
     */
    private String serviceIntro;
    /**
     * 业主心态
     */
    private String ownerFeel;
    /**
     * 建造年代
     */
    private String constructionAge;
    /**
     * 建筑形式
     */
    private String buildingTypes;
    /**
     * 售卖类型 0: 二手房 1: 新房
     */
    private Integer saleType;
    /**
     * 产权年限
     */
    private String propertyYears;
    /**
     * 是否为业主唯一住房 0：否 1：是
     */
    private Integer uniqueHouse;
    /**
     * 房屋年限
     */
    private String houseYearLimit;
    /**
     * 首付
     */
    private Double payinAdvance;
    /**
     * 佣金比例≤
     */
    private Double commission;
    /**
     * 备案编号
     */
    private String recordNumber;
    /**
     * 产权类型
     */
    private String propertyTypes;
    /**
     * 磐石楼栋户室号ID
     */
    private String panshiHouseId;
    /**
     * 房屋图片id
     */
    @TableField(exist=false)
    private List<BusinessFileSaveDTO> businessFileSaveDTOs;
}
