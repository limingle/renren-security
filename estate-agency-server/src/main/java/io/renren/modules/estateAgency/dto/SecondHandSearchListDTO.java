package io.renren.modules.estateAgency.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author limingle
 * @version 1.0.0
 * @ClassName SecondHandSearchListDTO.java
 * @Description TODO
 * @createTime 2021/1/13 17:43
 */
@Data
public class SecondHandSearchListDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 房屋主键
     */
    private Long id;
    /**
     * 小区id
     */
    private Long communityId;
    /**
     * 小区名称
     */
    private String communityName;
    /**
     * 小区类别
     */
    private Long areaCategory;
    /**
     * 楼层
     */
    private Long currentFloor;
    /**
     * 楼层（共）
     */
    private Long totalFloor;
    /**
     * 户型（室）
     */
    private Integer room;
    /**
     * 户型（厅）
     */
    private Integer hall;
    /**
     * 户型（卫）
     */
    private Integer bathroom;
    /**
     * 建筑面积
     */
    private Double constructionArea;
    /**
     * 标题
     */
    private String smartTitle;
    /**
     * 售价
     */
    private Double price;
    /**
     * 建造年代
     */
    private String constructionAge;
    /**
     * 小区图片列表
     */
    private List<Long> fileIds;
    /**
     * 创建人员
     */
    private Long createUser;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 1-上架，2-下架
     */
    private Integer state;
}
