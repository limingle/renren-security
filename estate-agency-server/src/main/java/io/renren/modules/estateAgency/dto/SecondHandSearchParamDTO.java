package io.renren.modules.estateAgency.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @title 二手房查询Dto
 * @description
 * @author limingle
 * @updateTime 2021/1/12 9:48
 * @throws
 */
@Data
public class SecondHandSearchParamDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 当前页数
     */
    private Integer pageNum;
    /**
     * 每页显示条数
     */
    private Integer pageSize;
}
