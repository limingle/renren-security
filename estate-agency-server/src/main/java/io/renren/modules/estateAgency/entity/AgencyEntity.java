package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@Data
@TableName("tb_agency")
public class AgencyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Integer id;
	/**
	 * 经纪机构名称
	 */
	private String name;
	/**
	 * 地址
	 */
	private String address;
	/**
	 * 手机
	 */
	private String phone;
	/**
	 * 电子邮件
	 */
	private String email;
	/**
	 * 描述
	 */
	private String aboutUs;
	/**
	 * 电话
	 */
	private String mobile;
	/**
	 * 网站
	 */
	private String webSite;

}
