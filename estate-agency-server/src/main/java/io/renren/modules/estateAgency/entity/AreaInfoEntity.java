package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 14:35:57
 */
@Data
@TableName("tb_area_info")
public class AreaInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Long id;
	/**
	 * 主标题
	 */
	private String mainTitle;
	/**
	 * 副标题
	 */
	private String subtitle;
	/**
	 * 小区类别
	 */
	private Long areaCategory;
	/**
	 * 建筑年代
	 */
	private String buildingYear;
	/**
	 * 建筑类型
	 */
	private String buildingType;
	/**
	 * 经度
	 */
	private Double lng;
	/**
	 * 纬度
	 */
	private Double lat;
	/**
	 * 物业费
	 */
	private String propertyCost;
	/**
	 * 物业公司
	 */
	private String propertyCompany;
	/**
	 * 开发商
	 */
	private String developer;
	/**
	 * 楼栋数量
	 */
	private Long buildingTotal;
	/**
	 * 房屋数量
	 */
	private Long houseTotal;
	/**
	 * 创建人员
	 */
	private Long createUser;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改人员
	 */
	private Long updateUser;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 小区介绍
	 */
	private String introduction;
	/**
	 * 小区配套
	 */
	private String supporting;
	/**
	 * 交通出行
	 */
	private String traffic;
	/**
	 * 删除标记 0：未删除  1：删除
	 */
	private Long isDelete;
	/**
	 * 小区图片id
	 */
	@TableField(exist=false)
	private List<Long> fileIds;

}
