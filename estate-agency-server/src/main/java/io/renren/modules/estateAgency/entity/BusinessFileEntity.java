package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@Data
@Builder
@TableName("tb_business_file")
public class BusinessFileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 业务数据主键
	 */
	private Long businessId;
	/**
	 * 文件主键
	 */
	private Long fileId;
	/**
	 * 排序
	 */
	private Integer sequence;
	/**
	 * 业务类型 0:小区图片 1:房屋图片 2:房本材料 3:人员头像 11:室 12:厅 13:厨 14:卫 15:阳台 16:户型图 17:室外图
	 */
	private Integer type;

}
