package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@Data
@TableName("tb_comment")
public class CommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 评论内容
	 */
	private String content;
	/**
	 * 房屋id
	 */
	private Long houseId;
	/**
	 * 发布时间戳
	 */
	private Date createTime;
	/**
	 * 父id  如果为跟则id为0
	 */
	private Long parentId;
	/**
	 * 类型1-房产评论，2-博客评论
	 */
	private Integer type;
	/**
	 * 评论用户
	 */
	private Long userId;
	/**
	 * 是否展示评论 0：不展示 1：展示 2：待审核
	 */
	private Integer isShow;

}
