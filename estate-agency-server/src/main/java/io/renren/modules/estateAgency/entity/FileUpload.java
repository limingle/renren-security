package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 上传附件
 *
 * @author limingle
 * @email limingle@outlook.com
 * @date 2020-07-16 16:54:03
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("TB_FILE_UPLOAD")
public class FileUpload implements Serializable {

    @TableId("FILE_ID")
    private Long fileId;
    @TableField("OPT_USER")
    private Long optUser;
    @TableField("FILE_SIZE")
    private Long fileSize;
    @TableField("FILE_NAME")
    private String fileName;
    @TableField("FORMAT")
    private String format;
    @TableField("LOCATION")
    private String location;
    @TableField("CREATE_DATE")
    private Date createDate;

}