package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@Data
@TableName("tb_hosuse_user")
public class HosuseUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 房屋id
	 */
	private Long houseId;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 1-售卖，2-收藏
	 */
	private Integer type;

}
