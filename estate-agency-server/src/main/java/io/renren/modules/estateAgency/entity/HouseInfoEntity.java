package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@Data
@TableName("tb_house_info")
public class HouseInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 房屋类型 1:销售，2:出租
	 */
	private Integer type;
	/**
	 * 主标题
	 */
	private String mainTitle;
	/**
	 * 副标题
	 */
	private String subtitle;
	/**
	 * 小区id外键
	 */
	private Long area;
	/**
	 * 价格
	 */
	private Double price;
	/**
	 * 卧室数量
	 */
	private Integer bedroom;
	/**
	 * 厅数量
	 */
	private Integer livingRoom;
	/**
	 * 厕所数量
	 */
	private Integer washroom;
	/**
	 * 厨房数量
	 */
	private Integer kitchen;
	/**
	 * 朝向
	 */
	private String direction;
	/**
	 * 建筑面积
	 */
	private Double buildingAcreage;
	/**
	 * 套内面积
	 */
	private Double insideAcreage;
	/**
	 * 楼层类型
	 */
	private String flootType;
	/**
	 * 楼层数量
	 */
	private Long flootCount;
	/**
	 * 房型
	 */
	private String roomType;
	/**
	 * 装修
	 */
	private String decoration;
	/**
	 * 建筑类型
	 */
	private String buildingType;
	/**
	 * 供暖方式
	 */
	private String heatingType;
	/**
	 * 产权
	 */
	private Integer equity;
	/**
	 * 建筑结构
	 */
	private String buildingStructure;
	/**
	 * 楼梯数量
	 */
	private Integer stair;
	/**
	 * 每层住户数量
	 */
	private Integer floorRoomCount;
	/**
	 * 是否有电梯 0：没有  1：有
	 */
	private Integer elevator;
	/**
	 * 创建人员
	 */
	private Long createUser;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改人员
	 */
	private Long updateUser;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 挂牌时间
	 */
	private Date saleTime;
	/**
	 * 上次交易时间
	 */
	private Date lastSaleTime;
	/**
	 * 抵押信息
	 */
	private String mortgageType;
	/**
	 * 交易权属
	 */
	private String ownershipType;
	/**
	 * 房屋用途
	 */
	private String use;
	/**
	 * 产权归属
	 */
	private String equityOwner;
	/**
	 * 核心卖点
	 */
	private String sellingPoint;
	/**
	 * 1-上架，2-下架
	 */
	private Integer state;

}
