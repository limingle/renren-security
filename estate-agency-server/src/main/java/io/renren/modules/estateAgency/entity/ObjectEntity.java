package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.estateAgency.dto.BusinessFileSaveDTO;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
@Data
@Builder
@TableName("tb_object")
public class ObjectEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 房屋主键
	 */
	@TableId
	private Long id;
	/**
	 * 房屋类型 1:销售，2:出租
	 */
	private Integer type;
	/**
	 * 小区id
	 */
	private Long communityId;
	/**
	 * 栋/幢/弄/胡同
	 */
	private Integer houseNumberBuilding;
	/**
	 * 单元/号
	 */
	private Integer houseNumberUnit;
	/**
	 * 门牌
	 */
	private Integer houseNumberRoom;
	/**
	 * 楼层（当前）
	 */
	private Long currentFloor;
	/**
	 * 楼层（共）
	 */
	private Long totalFloor;
	/**
	 * 实际楼层
	 */
	private Long actualFloor;
	/**
	 * 户型（室）
	 */
	private Integer room;
	/**
	 * 户型（厅）
	 */
	private Integer hall;
	/**
	 * 户型（卫）
	 */
	private Integer bathroom;
	/**
	 * 建筑面积
	 */
	private Double constructionArea;
	/**
	 * 套内面积
	 */
	private Double innerBuildingArea;
	/**
	 * 标题
	 */
	private String smartTitle;
	/**
	 * 主卧朝向
	 */
	private String bedroomOriented;
	/**
	 * 房屋类型
	 */
	private String houseTypes;
	/**
	 * 装修情况
	 */
	private String decorationStatus;
	/**
	 * 是否有电梯 0：没有  1：有
	 */
	private Integer elevator;
	/**
	 * 供暖
	 */
	private String heating;
	/**
	 * 房源详情
	 */
	private String houseDetail;
	/**
	 * 售价
	 */
	private Double price;
	/**
	 * 每层住户数量
	 */
	private Integer floorRoomCount;
	/**
	 * 创建人员
	 */
	private Long createUser;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改人员
	 */
	private Long updateUser;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 挂牌时间
	 */
	private Date saleTime;
	/**
	 * 摘牌时间
	 */
	private Date noSaleTime;
	/**
	 *
	 */
	private String homeowner;
	/**
	 * 房主电话
	 */
	private String homeownerPhone;
	/**
	 * 房主电话备用
	 */
	private String homeownerPhone1;
	/**
	 * 房主微信
	 */
	private String homeownerWx;
	/**
	 * 1-上架，2-下架
	 */
	private Integer state;
	/**
	 * 删除标记 0:未删除 1:删除
	 */
	private Integer isDelete;
	/**
	 * 房屋图片id
	 */
	@TableField(exist=false)
	private List<BusinessFileSaveDTO> businessFileSaveDTOS;

}
