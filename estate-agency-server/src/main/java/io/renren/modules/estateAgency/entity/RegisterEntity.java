package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
@Data
@TableName("tb_register")
public class RegisterEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 手机号
	 */
	private String phone;
	/**
	 * 电子邮件
	 */
	private String email;
	/**
	 * 自我介绍
	 */
	private String aboutme;
	/**
	 * 经过MD5加密的密码
	 */
	private String passwd;
	/**
	 * 1:普通用户，2:房产经纪人
	 */
	private Integer type;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 是否启用,1启用，0停用
	 */
	private Integer enable;
	/**
	 * 所属经纪机构
	 */
	private Integer agencyId;
	/**
	 * 所属经纪机构名称
	 */
	@TableField(exist=false)
	private String agencyName;
	/**
	 * 头像id
	 */
	@TableField(exist=false)
	private Long fileId;

}
