package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
@Data
@TableName("tb_rent")
public class RentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * object表外键关联
	 */
	private Long objectId;
	/**
	 * 出租方式 0:合租 1:整租
	 */
	private Integer rentType;
	/**
	 * 出租面积
	 */
	private Double rentArea;
	/**
	 * 磐石楼栋户室号ID
	 */
	private String panshiHouseid;
	/**
	 * 已出租数量
	 */
	private Integer rentNum;
	/**
	 * 待租卧室朝向
	 */
	private String roomOrient;
	/**
	 * 公共设施
	 */
	private String publicFacilities;
	/**
	 * 房间设施
	 */
	private String relatedFacilities;
	/**
	 * 共X户合租
	 */
	private Integer totalNum;
	/**
	 * 待租卧室
	 */
	private String roomMode;
	/**
	 * 付款方式
	 */
	private String priceMode;
	/**
	 * 最早入住时间
	 */
	private Date rentTime;
	/**
	 * 佣金
	 */
	private Double commission;
	/**
	 * 看房时间
	 */
	private String visitTime;
	/**
	 * 房屋亮点
	 */
	private String wholeHouseFeature;
	/**
	 * 房屋亮点
	 */
	private String shareHouseFeature;
	/**
	 * 费用明细
	 */
	private String feeDetail;
	/**
	 * 出租要求
	 */
	private String rentDemand;

}
