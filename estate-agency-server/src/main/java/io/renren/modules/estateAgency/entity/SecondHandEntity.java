package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
@Data
@Builder
@TableName("tb_second_hand")
public class SecondHandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * object表外键关联
	 */
	private Long objectId;
	/**
	 * 服务介绍
	 */
	private String serviceIntro;
	/**
	 * 业主心态
	 */
	private String ownerFeel;
	/**
	 * 建造年代
	 */
	private String constructionAge;
	/**
	 * 建筑形式
	 */
	private String buildingTypes;
	/**
	 * 售卖类型 0: 二手房 1: 新房
	 */
	private Integer saleType;
	/**
	 * 产权年限
	 */
	private String propertyYears;
	/**
	 * 是否为业主唯一住房 0：否 1：是
	 */
	private Integer uniqueHouse;
	/**
	 * 房屋年限
	 */
	private String houseYearLimit;
	/**
	 * 首付
	 */
	private Double payinAdvance;
	/**
	 * 佣金比例≤
	 */
	private Double commission;
	/**
	 * 备案编号
	 */
	private String recordNumber;
	/**
	 * 产权类型
	 */
	private String propertyTypes;
	/**
	 * 磐石楼栋户室号ID
	 */
	private String panshiHouseId;

}
