package io.renren.modules.estateAgency.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 标签管理
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-05-10 14:45:21
 */
@Data
@TableName("tb_tag")
public class TagEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 标签ID
	 */
	@TableId
	private Long tagId;
	/**
	 * 上级标签，一级分类为0
	 */
	private Long parentId;
	/**
	 * 上级标签名称
	 */
	@TableField(exist=false)
	private String parentName;
	/**
	 * 标签名称
	 */
	private String name;
	/**
	 * 字体颜色
	 */
	private String color;
	/**
	 * 背景颜色
	 */
	private String bgColor;
	/**
	 * 排序
	 */
	private Integer orderNum;
	/**
	 * 是否删除  -1：已删除  0：正常
	 */
	private Integer delFlag;
	/**
	 * 创建人
	 */
	private Long createUser;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 是否为自定义  1：自定义  0：非定义
	 */
	private Integer isCustom;
	/**
	 * ztree属性
	 */
	@TableField(exist=false)
	private Boolean open;
	@TableField(exist=false)
	private List<TagEntity> children;

}
