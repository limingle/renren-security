package io.renren.modules.estateAgency.enums;

/**
 * @author limingle
 * @version 1.0.0
 * @ClassName FileType.java
 * @Description TODO
 * @createTime 2020/11/2 16:56
 */
public enum FileType {
    AREA_FILE(0,"小区图片"),
    HOUSE_FILE(1,"房屋图片"),
    HOUSE_BOOK_FILE(2,"房本材料"),
    PERSON_FILE(3,"人员头像"),
    ROOM_FILE(11,"户型（室）"),
    HALL_FILE(12,"户型（厅）"),
    KITCHEN_FILE(13,"户型（厨）"),
    BATHROOM_FILE(14,"户型（卫）"),
    BALCONY_FILE(15,"阳台"),
    ROOM_TYPE_FILE(16,"户型图"),
    OUTSIZE_FILE(17,"室外图");

    private Integer code;
    private String name;
    FileType(Integer code,String name){
        this.code=code;
        this.name=name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
