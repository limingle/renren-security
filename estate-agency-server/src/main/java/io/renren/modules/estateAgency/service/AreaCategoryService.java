package io.renren.modules.estateAgency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.estateAgency.entity.AreaCategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 小区分类管理
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-03-27 16:54:03
 */
public interface AreaCategoryService extends IService<AreaCategoryEntity> {

    List<AreaCategoryEntity> queryList(Map<String, Object> map);

    List<AreaCategoryEntity> queryTree(Map<String, Object> map);

    /**
     * 查询子部门ID列表
     * @param parentId  上级部门ID
     */
    List<Long> queryCategoryIdList(Long parentId);

    /**
     * 获取子部门ID，用于数据过滤
     */
    List<Long> getSubCategoryIdList(Long categoryId);

    /**
     * 根据id获取父级Id
     * @param categoryId
     * @return
     */
    List<Long> getAreaCategoryParentId(Long categoryId);

    /**
     * 根据id获取父级
     * @param categoryId
     * @return
     */
    List<String> getAreaCategoryParent(Long categoryId);
}

