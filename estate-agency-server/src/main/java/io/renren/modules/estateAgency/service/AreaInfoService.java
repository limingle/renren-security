package io.renren.modules.estateAgency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.estateAgency.dto.AreaInfoSearchParamDTO;
import io.renren.modules.estateAgency.entity.AreaInfoEntity;
import io.renren.modules.estateAgency.vo.AreaInfoDetailVO;
import io.renren.modules.estateAgency.vo.AreaInfoSearchListVO;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 14:35:57
 */
public interface AreaInfoService extends IService<AreaInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<AreaInfoSearchListVO> queryAreaInfoSearchList(AreaInfoSearchParamDTO paramDTO);

    AreaInfoDetailVO getAreaInfoDetail(Long id);

    void saveAreaInfo(AreaInfoEntity areaInfo);

    void updateAreaInfo(AreaInfoEntity areaInfo);

    void removeAreaByIds(List<Long> asList);
}

