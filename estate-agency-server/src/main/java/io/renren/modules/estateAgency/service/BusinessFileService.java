package io.renren.modules.estateAgency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.estateAgency.entity.BusinessFileEntity;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
public interface BusinessFileService extends IService<BusinessFileEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<Long> queryFileIdsBybusinessId(Long businessId,Integer type);

    void saveFile(Long businessId, List<BusinessFileEntity> businessFiles);
}

