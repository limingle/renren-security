package io.renren.modules.estateAgency.service;

import io.renren.modules.estateAgency.entity.FileUpload;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;


public interface FileUploadService {

    boolean saveFile(InputStream in, long fileSize, String userCode, String saveFileName, FileUpload fileUpload);

    HashMap<String, Object> downloadFile(String fileId);

    List<FileUpload> fileUploadSearch(List<String> fileIds);

    List<FileUpload> selectList();

    FileUpload selectById(String id);

}