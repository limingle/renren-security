package io.renren.modules.estateAgency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.estateAgency.entity.ObjectEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
public interface ObjectService extends IService<ObjectEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveObject(ObjectEntity oe);

    void removeObjectByIds(@Param("asList") List<Long> asList);
}

