package io.renren.modules.estateAgency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.estateAgency.dto.RegisterSearchParamDTO;
import io.renren.modules.estateAgency.entity.RegisterEntity;
import io.renren.modules.estateAgency.vo.RegisterSearchListVO;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-09-23 21:06:28
 */
public interface RegisterService extends IService<RegisterEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<RegisterSearchListVO> queryRegisterList(RegisterSearchParamDTO paramDTO);

    void saveRegister(RegisterEntity register);

    void update(RegisterEntity register);
}
