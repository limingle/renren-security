package io.renren.modules.estateAgency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.estateAgency.entity.RentEntity;

import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
public interface RentService extends IService<RentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

