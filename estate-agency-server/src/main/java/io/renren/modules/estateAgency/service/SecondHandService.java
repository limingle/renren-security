package io.renren.modules.estateAgency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.estateAgency.dto.SecondHandSaveDTO;
import io.renren.modules.estateAgency.dto.SecondHandSearchParamDTO;
import io.renren.modules.estateAgency.entity.SecondHandEntity;
import io.renren.modules.estateAgency.vo.SecondHandDetailVO;
import io.renren.modules.estateAgency.vo.SecondHandSearchListVO;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-01-11 15:00:00
 */
public interface SecondHandService extends IService<SecondHandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SecondHandSearchListVO> querySecondHandSearchList(SecondHandSearchParamDTO paramDTO);

    void saveSecondHand(SecondHandSaveDTO secondHand);

    void removeSecondHandByIds(List<Long> asList);

    SecondHandDetailVO getSecondHandById(Long id);
}

