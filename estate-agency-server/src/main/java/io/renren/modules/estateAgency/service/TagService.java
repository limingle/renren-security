package io.renren.modules.estateAgency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.estateAgency.entity.TagEntity;

import java.util.List;
import java.util.Map;

/**
 * 标签管理
 *
 * @author Mark
 * @email limingle@outlook.com
 * @date 2021-05-10 14:45:21
 */
public interface TagService extends IService<TagEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<TagEntity> queryList(Map<String, Object> params);

    List<TagEntity> queryTree();
    /**
     * 查询子部门ID列表
     * @param parentId  上级部门ID
     */
    List<Long> queryTagIdList(Long parentId);

    void deleteTag(Long tagId);
}

