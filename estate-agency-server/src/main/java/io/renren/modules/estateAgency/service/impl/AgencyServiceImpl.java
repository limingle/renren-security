package io.renren.modules.estateAgency.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.estateAgency.dao.AgencyDao;
import io.renren.modules.estateAgency.entity.AgencyEntity;
import io.renren.modules.estateAgency.service.AgencyService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("agencyService")
public class AgencyServiceImpl extends ServiceImpl<AgencyDao, AgencyEntity> implements AgencyService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AgencyEntity> page = this.page(
                new Query<AgencyEntity>().getPage(params),
                new QueryWrapper<AgencyEntity>()
        );

        return new PageUtils(page);
    }

}
