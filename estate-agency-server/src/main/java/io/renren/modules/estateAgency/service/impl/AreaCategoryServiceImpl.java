package io.renren.modules.estateAgency.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.annotation.DataFilter;
import io.renren.modules.estateAgency.dao.AreaCategoryDao;
import io.renren.modules.estateAgency.entity.AreaCategoryEntity;
import io.renren.modules.estateAgency.service.AreaCategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("areaCategoryService")
public class AreaCategoryServiceImpl extends ServiceImpl<AreaCategoryDao, AreaCategoryEntity> implements AreaCategoryService {

    @Override
    @DataFilter(subDept = true, user = false, tableAlias = "t1")
    public List<AreaCategoryEntity> queryList(Map<String, Object> params){
        return baseMapper.queryList(params);
    }

    @Override
    public List<AreaCategoryEntity> queryTree(Map<String, Object> map) {
        List<AreaCategoryEntity> list = this.queryList(map);
        List<AreaCategoryEntity> trees = new ArrayList<AreaCategoryEntity>();

        for (AreaCategoryEntity treeNode : list) {

            if (treeNode.getParentId().equals(0L)) {
                trees.add(treeNode);
            }

            for (AreaCategoryEntity it : list) {
                if (it.getParentId() == treeNode.getCategoryId()) {
                    if (treeNode.getChildren() == null) {
                        treeNode.setChildren(new ArrayList<AreaCategoryEntity>());
                    }
                    treeNode.getChildren().add(it);
                }
            }
        }
        return trees;
    }

    @Override
    public List<Long> queryCategoryIdList(Long parentId) {
        return baseMapper.queryCategoryIdList(parentId);
    }

    @Override
    public List<Long> getSubCategoryIdList(Long deptId){
        //部门及子部门ID列表
        List<Long> categoryIdList = new ArrayList<>();

        //获取子部门ID
        List<Long> subIdList = queryCategoryIdList(deptId);
        getCategoryTreeList(subIdList, categoryIdList);

        return categoryIdList;
    }

    @Override
    public List<Long> getAreaCategoryParentId(Long categoryId) {
        List<Map<String,Object>> listMap = baseMapper.getAreaCategoryParentId(categoryId);
        List<Long> areaCategoryIds=new ArrayList<>();
        if (listMap.get(0).get("name1")!=null){
            areaCategoryIds.add((Long)listMap.get(0).get("name1"));
        }
        if (listMap.get(0).get("name2")!=null){
            areaCategoryIds.add((Long)listMap.get(0).get("name2"));
        }
        if (listMap.get(0).get("name3")!=null){
            areaCategoryIds.add((Long)listMap.get(0).get("name3"));
        }
        return areaCategoryIds;
    }

    @Override
    public List<String> getAreaCategoryParent(Long categoryId) {
        List<Map<String,Object>> listMap = baseMapper.getAreaCategoryParent(categoryId);
        List<String> areaCategoryNames=new ArrayList<>();
        if (listMap.get(0).get("name1")!=null){
            areaCategoryNames.add((String)listMap.get(0).get("name1"));
        }
        if (listMap.get(0).get("name2")!=null){
            areaCategoryNames.add((String)listMap.get(0).get("name2"));
        }
        if (listMap.get(0).get("name3")!=null){
            areaCategoryNames.add((String)listMap.get(0).get("name3"));
        }
        return areaCategoryNames;
    }

    /**
     * 递归
     */
    private void getCategoryTreeList(List<Long> subIdList, List<Long> categoryIdList){
        for(Long categoryId : subIdList){
            List<Long> list = queryCategoryIdList(categoryId);
            if(list.size() > 0){
                getCategoryTreeList(list, categoryIdList);
            }

            categoryIdList.add(categoryId);
        }
    }

}
