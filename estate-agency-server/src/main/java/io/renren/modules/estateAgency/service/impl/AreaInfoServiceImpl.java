package io.renren.modules.estateAgency.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.estateAgency.dao.AreaInfoDao;
import io.renren.modules.estateAgency.dto.AreaInfoSearchParamDTO;
import io.renren.modules.estateAgency.entity.AreaInfoEntity;
import io.renren.modules.estateAgency.entity.BusinessFileEntity;
import io.renren.modules.estateAgency.enums.FileType;
import io.renren.modules.estateAgency.service.AreaInfoService;
import io.renren.modules.estateAgency.service.BusinessFileService;
import io.renren.modules.estateAgency.vo.AreaInfoDetailVO;
import io.renren.modules.estateAgency.vo.AreaInfoSearchListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("areaInfoService")
public class AreaInfoServiceImpl extends ServiceImpl<AreaInfoDao, AreaInfoEntity> implements AreaInfoService {

    @Autowired
    BusinessFileService businessFileService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AreaInfoEntity> page = this.page(
                new Query<AreaInfoEntity>().getPage(params),
                new QueryWrapper<AreaInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<AreaInfoSearchListVO> queryAreaInfoSearchList(AreaInfoSearchParamDTO paramDTO) {
        return baseMapper.queryAreaInfoSearchList(paramDTO);
    }

    @Override
    public AreaInfoDetailVO getAreaInfoDetail(Long id) {
        return baseMapper.getAreaInfoDetail(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveAreaInfo(AreaInfoEntity areaInfo) {
        this.save(areaInfo);
        List<Long> fieldIds = areaInfo.getFileIds();
        if (fieldIds == null || fieldIds.size() == 0) {
            return;
        }
        this.saveFile(areaInfo.getId(), fieldIds);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateAreaInfo(AreaInfoEntity areaInfo) {
        this.updateById(areaInfo);
        List<Long> fieldIds = areaInfo.getFileIds();
        QueryWrapper<BusinessFileEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", areaInfo.getId());
        List<BusinessFileEntity> businessFiles = businessFileService.list(queryWrapper);
        if (businessFiles != null || businessFiles.size() > 0) {
            businessFileService.remove(queryWrapper);
        }
        this.saveFile(areaInfo.getId(), fieldIds);
    }

    @Override
    public void removeAreaByIds(List<Long> asList) {
        baseMapper.removeAreaByIds(asList);
    }

    private void saveFile(Long businessId,List<Long> fieldIds){
        List<BusinessFileEntity> businessFileEntities =fieldIds.stream().map(fieldId->{
            return BusinessFileEntity.builder()
                    .fileId(fieldId)
                    .type(FileType.AREA_FILE.getCode())
                    .build();
        }).collect(Collectors.toList());
        businessFileService.saveFile(businessId, businessFileEntities);
    }

}
