package io.renren.modules.estateAgency.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.estateAgency.dao.BusinessFileDao;
import io.renren.modules.estateAgency.entity.BusinessFileEntity;
import io.renren.modules.estateAgency.service.BusinessFileService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("businessFileService")
public class BusinessFileServiceImpl extends ServiceImpl<BusinessFileDao, BusinessFileEntity> implements BusinessFileService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<BusinessFileEntity> page = this.page(
                new Query<BusinessFileEntity>().getPage(params),
                new QueryWrapper<BusinessFileEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<Long> queryFileIdsBybusinessId(Long businessId,Integer type) {
        return baseMapper.queryFileIdsBybusinessId(businessId,type);
    }

    @Override
    public void saveFile(Long businessId, List<BusinessFileEntity> businessFiles) {
        for (int i = 0; i < businessFiles.size(); i++) {
            businessFiles.get(i).setBusinessId(businessId);
            businessFiles.get(i).setSequence(i);
        }
        this.saveBatch(businessFiles);
    }

}
