package io.renren.modules.estateAgency.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.FileUploadUtil;
import io.renren.modules.estateAgency.controller.FileUploadController;
import io.renren.modules.estateAgency.dao.FileUploadMapper;
import io.renren.modules.estateAgency.entity.FileUpload;
import io.renren.modules.estateAgency.service.FileUploadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

@Service
public class FileUploadServiceImpl extends ServiceImpl<FileUploadMapper, FileUpload> implements FileUploadService {

    private static Logger logger = LoggerFactory.getLogger(FileUploadController.class);

    @Value("${renren.uploadDir}")
    private String uploadDir;

    @Override
    @Transactional
    public boolean saveFile(InputStream in, long fileSize, String userId, String saveFileName, FileUpload fileUpload) {
        //保存路径,用户id开头,后面跟上年月日
        Calendar now = Calendar.getInstance();
        String fileSavePath = uploadDir + File.separator + userId + File.separator + now.get(Calendar.YEAR) + File.separator + (now.get(Calendar.MONTH) + 1) + File.separator + now.get(Calendar.DAY_OF_MONTH) + File.separator + saveFileName;
        fileUpload.setLocation(fileSavePath);
        this.save(fileUpload);
        long saveSize = saveFileByInputStream(in, fileSavePath);
        if (saveSize == fileSize) {
            return true;
        }
        return false;
    }


    /**
     * 输入流保存文件
     *
     * @param in
     * @return
     */
    private long saveFileByInputStream(InputStream in, String filePath) {
        long saveFileSize = 0L;
        try {
            saveFileSize = FileUploadUtil.saveFile(in, filePath);
        } catch (IOException e) {
            logger.error("saveFileByInputStream 出现异常!", e);
        }
        return saveFileSize;
    }


    public HashMap<String, Object> downloadFile(String fileId) {
        HashMap<String, Object> returnMap = new HashMap<>();
        try {
            FileUpload fileBean = this.selectById(fileId);
            InputStream in = new FileInputStream(fileBean.getLocation());
            returnMap.put("in", in);
            returnMap.put("fileName", fileBean.getFileName());
        } catch (IOException e) {
            logger.error("获取下载文件出现异常!", e);
        }
        return returnMap;
    }


    @Override
    public List<FileUpload> fileUploadSearch(List<String> ids) {
        List<FileUpload> list = new ArrayList<>();
        if (ids != null && ids.size() > 0) {
            list = (List<FileUpload>) this.listByIds(ids);
        }
        return list;
    }

    @Override
    public List<FileUpload> selectList() {
        return baseMapper.selectList(null);
    }

    @Override
    public FileUpload selectById(String id) {
        return baseMapper.selectById(id);
    }

}

