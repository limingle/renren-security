package io.renren.modules.estateAgency.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.estateAgency.dao.HosuseUserDao;
import io.renren.modules.estateAgency.entity.HosuseUserEntity;
import io.renren.modules.estateAgency.service.HosuseUserService;


@Service("hosuseUserService")
public class HosuseUserServiceImpl extends ServiceImpl<HosuseUserDao, HosuseUserEntity> implements HosuseUserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<HosuseUserEntity> page = this.page(
                new Query<HosuseUserEntity>().getPage(params),
                new QueryWrapper<HosuseUserEntity>()
        );

        return new PageUtils(page);
    }

}
