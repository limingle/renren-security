package io.renren.modules.estateAgency.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.estateAgency.dao.ObjectDao;
import io.renren.modules.estateAgency.entity.BusinessFileEntity;
import io.renren.modules.estateAgency.entity.ObjectEntity;
import io.renren.modules.estateAgency.service.BusinessFileService;
import io.renren.modules.estateAgency.service.ObjectService;
import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("objectService")
public class ObjectServiceImpl extends ServiceImpl<ObjectDao, ObjectEntity> implements ObjectService {

    @Autowired
    BusinessFileService businessFileService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ObjectEntity> page = this.page(
                new Query<ObjectEntity>().getPage(params),
                new QueryWrapper<ObjectEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveObject(ObjectEntity oe) {
        Long userId = ((SysUserEntity) SecurityUtils.getSubject().getPrincipal()).getUserId();
        if (oe.getId() == null) {
            oe.setCreateUser(userId);
            oe.setCreateTime(new Date());
        }else{
            oe.setUpdateUser(userId);
            oe.setUpdateTime(new Date());
            this.removeAllBusinessFile(oe.getId());
        }
        oe.setType(1);
        this.saveOrUpdate(oe);
    }

    @Override
    public void removeObjectByIds(List<Long> asList) {
        baseMapper.removeObjectByIds(asList);
    }

    private void removeAllBusinessFile(Long businessId){
        QueryWrapper<BusinessFileEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", businessId);
        List<BusinessFileEntity> businessFiles = businessFileService.list(queryWrapper);
        if (businessFiles != null || businessFiles.size() > 0) {
            businessFileService.remove(queryWrapper);
        }
    }

    private void saveFile(Long businessId,List<BusinessFileEntity> businessFileEntities){
        businessFileService.saveFile(businessId, businessFileEntities);
    }

}
