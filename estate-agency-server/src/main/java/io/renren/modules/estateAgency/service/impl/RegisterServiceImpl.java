package io.renren.modules.estateAgency.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.estateAgency.dao.RegisterDao;
import io.renren.modules.estateAgency.dto.RegisterSearchParamDTO;
import io.renren.modules.estateAgency.entity.BusinessFileEntity;
import io.renren.modules.estateAgency.entity.RegisterEntity;
import io.renren.modules.estateAgency.enums.FileType;
import io.renren.modules.estateAgency.service.BusinessFileService;
import io.renren.modules.estateAgency.service.RegisterService;
import io.renren.modules.estateAgency.vo.RegisterSearchListVO;
import io.renren.modules.sys.shiro.ShiroUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service("registerService")
public class RegisterServiceImpl extends ServiceImpl<RegisterDao, RegisterEntity> implements RegisterService {

    @Value("${renren.defaultPassword}")
    private String defaultPassword;

    @Autowired
    BusinessFileService businessFileService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RegisterEntity> page = this.page(
                new Query<RegisterEntity>().getPage(params),
                new QueryWrapper<RegisterEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<RegisterSearchListVO> queryRegisterList(RegisterSearchParamDTO paramDTO) {
        return baseMapper.queryRegisterList(paramDTO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveRegister(RegisterEntity register) {
        String salt = RandomStringUtils.randomAlphanumeric(20);
        register.setPasswd(ShiroUtils.sha256(defaultPassword, salt));
        this.save(register);
        Long fieldId = register.getFileId();
        if (register.getFileId() != null) {
            this.saveFile(register.getId(), fieldId);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(RegisterEntity register) {
        this.updateById(register);
        Long fieldId = register.getFileId();
        if (register.getFileId() != null) {
            QueryWrapper<BusinessFileEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", register.getId());
            BusinessFileEntity businessFile = businessFileService.getOne(queryWrapper);
            if (businessFile == null) {
                this.saveFile(register.getId(), fieldId);
            } else if (businessFile.getFileId() != register.getFileId()) {
                businessFileService.removeById(businessFile.getId());
                this.saveFile(register.getId(), fieldId);
            }
        }
    }

    private void saveFile(Long businessId, Long fileId) {
        BusinessFileEntity businessFile = BusinessFileEntity.builder()
                .fileId(fileId)
                .type(FileType.PERSON_FILE.getCode())
                .businessId(businessId)
                .sequence(0)
                .build();
        businessFileService.save(businessFile);
    }

}
