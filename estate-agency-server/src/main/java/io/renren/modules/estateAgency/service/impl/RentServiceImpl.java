package io.renren.modules.estateAgency.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.estateAgency.dao.RentDao;
import io.renren.modules.estateAgency.entity.RentEntity;
import io.renren.modules.estateAgency.service.RentService;


@Service("rentService")
public class RentServiceImpl extends ServiceImpl<RentDao, RentEntity> implements RentService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RentEntity> page = this.page(
                new Query<RentEntity>().getPage(params),
                new QueryWrapper<RentEntity>()
        );

        return new PageUtils(page);
    }

}
