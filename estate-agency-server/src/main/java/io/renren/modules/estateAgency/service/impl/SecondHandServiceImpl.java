package io.renren.modules.estateAgency.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.estateAgency.dao.SecondHandDao;
import io.renren.modules.estateAgency.dto.SecondHandSaveDTO;
import io.renren.modules.estateAgency.dto.SecondHandSearchListDTO;
import io.renren.modules.estateAgency.dto.SecondHandSearchParamDTO;
import io.renren.modules.estateAgency.entity.BusinessFileEntity;
import io.renren.modules.estateAgency.entity.ObjectEntity;
import io.renren.modules.estateAgency.entity.SecondHandEntity;
import io.renren.modules.estateAgency.enums.FileType;
import io.renren.modules.estateAgency.service.AreaCategoryService;
import io.renren.modules.estateAgency.service.BusinessFileService;
import io.renren.modules.estateAgency.service.ObjectService;
import io.renren.modules.estateAgency.service.SecondHandService;
import io.renren.modules.estateAgency.vo.SecondHandDetailVO;
import io.renren.modules.estateAgency.vo.SecondHandSearchListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;


@Service("secondHandService")
public class SecondHandServiceImpl extends ServiceImpl<SecondHandDao, SecondHandEntity> implements SecondHandService {

    @Autowired
    private BusinessFileService businessFileService;
    @Autowired
    private AreaCategoryService areaCategoryService;
    @Autowired
    private ObjectService objectService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SecondHandEntity> page = this.page(
                new Query<SecondHandEntity>().getPage(params),
                new QueryWrapper<SecondHandEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<SecondHandSearchListVO> querySecondHandSearchList(SecondHandSearchParamDTO paramDTO) {
        PageHelper.startPage(paramDTO.getPageNum(),paramDTO.getPageSize());
        List<SecondHandSearchListDTO> list= baseMapper.querySecondHandSearchList(paramDTO);
        List<SecondHandSearchListVO> result= list.stream().map(secondHandSearchListDTO->{
            String currentFloorVirtual="";
            if (secondHandSearchListDTO.getCurrentFloor()!=null && secondHandSearchListDTO.getTotalFloor()!=null){
                Float cfv=secondHandSearchListDTO.getCurrentFloor().floatValue()/secondHandSearchListDTO.getTotalFloor();
                if (cfv<0.34){
                    currentFloorVirtual="低楼层";
                }else if (cfv>=0.34 &&cfv<0.67){
                    currentFloorVirtual="中楼层";
                }else {
                    currentFloorVirtual="高楼层";
                }
            }
            List<Long> fileIds = businessFileService.queryFileIdsBybusinessId(secondHandSearchListDTO.getId(), FileType.AREA_FILE.getCode());
            List<String> areaCategoryStringList=(areaCategoryService.getAreaCategoryParent(secondHandSearchListDTO.getAreaCategory()));
            return SecondHandSearchListVO.builder()
                    .areaCategory(secondHandSearchListDTO.getAreaCategory())
                    .areaCategoryStringArr(areaCategoryStringList)
                    .bathroom(secondHandSearchListDTO.getBathroom())
                    .communityId(secondHandSearchListDTO.getCommunityId())
                    .communityName(secondHandSearchListDTO.getCommunityName())
                    .constructionAge(secondHandSearchListDTO.getConstructionAge())
                    .constructionArea(secondHandSearchListDTO.getConstructionArea())
                    .createTime(secondHandSearchListDTO.getCreateTime())
                    .createUser(secondHandSearchListDTO.getCreateUser())
                    .currentFloorVirtual(currentFloorVirtual)
                    .hall(secondHandSearchListDTO.getHall())
                    .id(secondHandSearchListDTO.getId())
                    .price(secondHandSearchListDTO.getPrice())
                    .room(secondHandSearchListDTO.getRoom())
                    .smartTitle(secondHandSearchListDTO.getSmartTitle())
                    .state(secondHandSearchListDTO.getState())
                    .totalFloor(secondHandSearchListDTO.getTotalFloor())
                    .fileIds(fileIds)
                    .build();
        }).collect(toList());
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveSecondHand(SecondHandSaveDTO secondHand) {
        ObjectEntity oe=ObjectEntity.builder()
                .actualFloor(secondHand.getActualFloor())
                .bathroom(secondHand.getBathroom())
                .bedroomOriented(secondHand.getBedroomOriented())
                .communityId(secondHand.getCommunityId())
                .constructionArea(secondHand.getConstructionArea())
                .currentFloor(secondHand.getCurrentFloor())
                .decorationStatus(secondHand.getDecorationStatus())
                .elevator(secondHand.getElevator())
                .floorRoomCount(secondHand.getFloorRoomCount())
                .hall(secondHand.getHall())
                .heating(secondHand.getHeating())
                .homeowner(secondHand.getHomeowner())
                .homeownerPhone(secondHand.getHomeownerPhone())
                .homeownerPhone1(secondHand.getHomeownerPhone1())
                .homeownerWx(secondHand.getHomeownerWx())
                .houseDetail(secondHand.getHouseDetail())
                .houseNumberBuilding(secondHand.getHouseNumberBuilding())
                .houseNumberRoom(secondHand.getHouseNumberRoom())
                .houseNumberUnit(secondHand.getHouseNumberUnit())
                .houseTypes(secondHand.getHouseTypes())
                .id(secondHand.getId())
                .innerBuildingArea(secondHand.getInnerBuildingArea())
                .price(secondHand.getPrice())
                .room(secondHand.getRoom())
                .smartTitle(secondHand.getSmartTitle())
                .totalFloor(secondHand.getTotalFloor())
                .businessFileSaveDTOS(secondHand.getBusinessFileSaveDTOs())
                .build();
        objectService.saveObject(oe);
        Long objectId=oe.getId();
        SecondHandEntity se=SecondHandEntity.builder()
                .buildingTypes(secondHand.getBuildingTypes())
                .commission(secondHand.getCommission())
                .constructionAge(secondHand.getConstructionAge())
                .houseYearLimit(secondHand.getHouseYearLimit())
                .objectId(objectId)
                .ownerFeel(secondHand.getOwnerFeel())
                .panshiHouseId(secondHand.getPanshiHouseId())
                .payinAdvance(secondHand.getPayinAdvance())
                .propertyTypes(secondHand.getPropertyTypes())
                .propertyYears(secondHand.getPropertyYears())
                .recordNumber(secondHand.getRecordNumber())
                .saleType(0)
                .serviceIntro(secondHand.getServiceIntro())
                .uniqueHouse(secondHand.getUniqueHouse())
                .build();
        this.saveOrUpdate(se);
    }

    @Override
    public void removeSecondHandByIds(List<Long> asList) {
        objectService.removeObjectByIds(asList);
    }

    @Override
    public SecondHandDetailVO getSecondHandById(Long id) {
        SecondHandDetailVO info= baseMapper.querySecondHandDetail(id);
        QueryWrapper<BusinessFileEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", id);
        List<BusinessFileEntity> businessFileEntities = businessFileService.list(queryWrapper);
        info.setBusinessFileEntities(businessFileEntities);
        return info;
    }

}
