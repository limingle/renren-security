package io.renren.modules.estateAgency.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.estateAgency.dao.TagDao;
import io.renren.modules.estateAgency.entity.TagEntity;
import io.renren.modules.estateAgency.service.TagService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("tagService")
public class TagServiceImpl extends ServiceImpl<TagDao, TagEntity> implements TagService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TagEntity> page = this.page(
                new Query<TagEntity>().getPage(params),
                new QueryWrapper<TagEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<TagEntity> queryList(Map<String, Object> params) {
        return baseMapper.queryList(params);
    }

    @Override
    public List<TagEntity> queryTree() {
        List<TagEntity> list = this.queryList(new HashMap<String, Object>());
        List<TagEntity> trees = new ArrayList<>();
        for (TagEntity treeNode : list) {
            if (treeNode.getParentId().equals(0l)) {
                trees.add(treeNode);
            }
            for (TagEntity it : list) {
                if (it.getParentId() == treeNode.getTagId()) {
                    if (treeNode.getChildren() == null) {
                        treeNode.setChildren(new ArrayList<TagEntity>());
                    }
                    treeNode.getChildren().add(it);
                }
            }
        }
        return trees;
    }

    @Override
    public List<Long> queryTagIdList(Long parentId) {
        return null;
    }

    @Override
    public void deleteTag(Long tagId) {
        TagEntity tag = new TagEntity();
        tag.setTagId(tagId);
        tag.setDelFlag(1);
        this.updateById(tag);
    }

}
