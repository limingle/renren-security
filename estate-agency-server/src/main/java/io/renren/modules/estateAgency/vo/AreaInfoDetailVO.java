package io.renren.modules.estateAgency.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author limingle
 * @version 1.0.0
 * @ClassName AreaInfoVO.java
 * @Description TODO
 * @createTime 2020/10/20 17:07
 */
@Data
public class AreaInfoDetailVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private Long id;
    /**
     * 主标题
     */
    private String mainTitle;
    /**
     * 副标题
     */
    private String subtitle;
    /**
     * 小区类别
     */
    private Long areaCategory;
    /**
     * 小区类别
     */
    private String areaCategoryString;
    /**
     * 小区类别分为数组后
     */
    private List<Long> areaCategoryIdArr;
    /**
     * 小区类别分为数组后
     */
    private List<String> areaCategoryStringArr;
    /**
     * 建筑年代
     */
    private String buildingYear;
    /**
     * 建筑类型
     */
    private String buildingType;
    /**
     * 建筑类型中文
     */
    private String buildingTypeString;
    /**
     * 经度
     */
    private Double lng;
    /**
     * 纬度
     */
    private Double lat;
    /**
     * 物业费
     */
    private String propertyCost;
    /**
     * 物业公司
     */
    private String propertyCompany;
    /**
     * 开发商
     */
    private String developer;
    /**
     * 楼栋数量
     */
    private Long buildingTotal;
    /**
     * 房屋数量
     */
    private Long houseTotal;
    /**
     * 创建人员
     */
    private Long createUser;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改人员
     */
    private Long updateUser;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 小区介绍
     */
    private String introduction;
    /**
     * 小区配套
     */
    private String supporting;
    /**
     * 交通出行
     */
    private String traffic;
    /**
     * 平均价格
     */
    private Long averagePrice;
    /**
     * 在售数量
     */
    private Long saleCount;
    /**
     * 小区图片列表
     */
    private List<Long> fileIds;
}
