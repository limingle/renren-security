package io.renren.modules.estateAgency.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author limingle
 * @version 1.0.0
 * @ClassName AreaInfoSearchListVO.java
 * @Description TODO
 * @createTime 2020/10/16 17:01
 */
@Data
public class AreaInfoSearchListVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;
    /**
     * 主标题
     */
    private String mainTitle;
    /**
     * 副标题
     */
    private String subtitle;
    /**
     * 小区类别
     */
    private Long areaCategory;
    /**
     * 小区类别
     */
    private String areaCategoryString;
    /**
     * 小区类别分为数组后
     */
    private List<String> areaCategoryStringArr;
    /**
     * 建筑年代
     */
    private String buildingYear;
    /**
     * 建筑类型
     */
    private String buildingType;
    /**
     * 建筑类型中文
     */
    private String buildingTypeString;
    /**
     * 开发商
     */
    private String developer;
    /**
     * 楼栋数量
     */
    private Long buildingTotal;
    /**
     * 房屋数量
     */
    private Long houseTotal;
    /**
     * 小区介绍
     */
    private String introduction;
    /**
     * 平均价格
     */
    private Long averagePrice;
    /**
     * 在售数量
     */
    private Long saleCount;
    /**
     * 小区图片列表
     */
    private List<Long> fileIds;
}
