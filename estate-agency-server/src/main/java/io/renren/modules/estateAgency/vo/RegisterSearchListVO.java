package io.renren.modules.estateAgency.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author limingle
 * @version 1.0.0
 * @ClassName RegisterSearchListVO.java
 * @Description 经纪人查询列表VO
 * @createTime 2020/9/30 16:47
 */
@Data
public class RegisterSearchListVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 电子邮件
     */
    private String email;
    /**
     * 自我介绍
     */
    private String aboutme;
    /**
     * 1:普通用户，2:房产经纪人
     */
    private Integer type;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 是否启用,1启用，0停用
     */
    private Integer enable;
    /**
     * 所属经纪机构
     */
    private Integer agencyId;
    /**
     * 所属经纪机构名称
     */
    private String agencyName;
    /**
     * 头像id
     */
    private Long fileId;
}
