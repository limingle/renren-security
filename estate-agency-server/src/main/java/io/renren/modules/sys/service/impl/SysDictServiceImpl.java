/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.sys.dao.SysDictDao;
import io.renren.modules.sys.dto.SysDictDTO;
import io.renren.modules.sys.dto.SysDictDetailDTO;
import io.renren.modules.sys.entity.SysDictEntity;
import io.renren.modules.sys.service.SysDictService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("sysDictService")
public class SysDictServiceImpl extends ServiceImpl<SysDictDao, SysDictEntity> implements SysDictService {

    @Override
    public List<SysDictDTO> queryList(Map<String, Object> params) {
        String name = (String) params.get("name");

        return  baseMapper.queryList(name);

//        IPage<SysDictEntity> page = this.page(
//                new Query<SysDictEntity>().getPage(params),
//                new QueryWrapper<SysDictEntity>()
//                        .select("DISTINCT type,`name`,remark")
//                        .like(StringUtils.isNotBlank(name), "name", name)
//        );
//
//        return new PageUtils(page);
    }

    @Override
    public Map<String, Object> getDicListByType(String type) {
        List<SysDictEntity> dictEntityList=baseMapper.queryListByType(type);
        if (dictEntityList.size() == 0) {
            return null;
        }
        SysDictEntity dict = dictEntityList.get(0);
        Map<String, Object> result = new HashMap<>();
        result.put("name",dict.getName());
        result.put("type",dict.getType());
        result.put("remark",dict.getRemark());
        List<SysDictDetailDTO> sysDictDetailDTOs = new ArrayList<>();
        for (SysDictEntity sde : dictEntityList) {
            SysDictDetailDTO sdde = new SysDictDetailDTO();
            sdde.setCode(sde.getCode());
            sdde.setDelFlag(sde.getDelFlag());
            sdde.setOrderNum(sde.getOrderNum());
            sdde.setValue(sde.getValue());
            sysDictDetailDTOs.add(sdde);
        }
        result.put("dictDetailList",sysDictDetailDTOs);
        return result;
    }

    @Override
    public Integer deleteDictByType(List<String> types) {
//        baseMapper.
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("types", types);
        return baseMapper.deleteDictByType(map);
    }

    @Override
    public List<SysDictEntity> getDicListByTypes(List<String> types) {
        List<SysDictEntity> dictEntityList=baseMapper.queryListByTypes(types);
        return dictEntityList;
    }

}
