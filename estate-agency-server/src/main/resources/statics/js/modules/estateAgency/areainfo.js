$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'estateAgency/areainfo/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: '主标题', name: 'mainTitle', index: 'main_title', width: 80 }, 			
			{ label: '副标题', name: 'subtitle', index: 'subtitle', width: 80 }, 			
			{ label: '小区类别', name: 'areaCategory', index: 'area_category', width: 80 }, 			
			{ label: '建筑类型', name: 'buildingType', index: 'building_type', width: 80 }, 			
			{ label: '经度', name: 'lng', index: 'lng', width: 80 }, 			
			{ label: '纬度', name: 'lat', index: 'lat', width: 80 }, 			
			{ label: '物业费', name: 'propertyCost', index: 'property_cost', width: 80 }, 			
			{ label: '物业公司', name: 'propertyCompany', index: 'property_company', width: 80 }, 			
			{ label: '开发商', name: 'developer', index: 'developer', width: 80 }, 			
			{ label: '楼栋数量', name: 'buildingTotal', index: 'building_total', width: 80 }, 			
			{ label: '房屋数量', name: 'houseTotal', index: 'house_total', width: 80 }, 			
			{ label: '创建人员', name: 'createUser', index: 'create_user', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }, 			
			{ label: '修改人员', name: 'updateUser', index: 'update_user', width: 80 }, 			
			{ label: '修改时间', name: 'updateTime', index: 'update_time', width: 80 }, 			
			{ label: '小区介绍', name: 'introduction', index: 'introduction', width: 80 }, 			
			{ label: '小区配套', name: 'supporting', index: 'supporting', width: 80 }, 			
			{ label: '交通出行', name: 'traffic', index: 'traffic', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		areaInfo: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.areaInfo = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.areaInfo.id == null ? "estateAgency/areainfo/save" : "estateAgency/areainfo/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.areaInfo),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "estateAgency/areainfo/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "estateAgency/areainfo/info/"+id, function(r){
                vm.areaInfo = r.areaInfo;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});