$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'estateAgency/houseinfo/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: '房屋类型 1:销售，2:出租', name: 'type', index: 'type', width: 80 }, 			
			{ label: '主标题', name: 'mainTitle', index: 'main_title', width: 80 }, 			
			{ label: '副标题', name: 'subtitle', index: 'subtitle', width: 80 }, 			
			{ label: '小区id外键', name: 'area', index: 'area', width: 80 }, 			
			{ label: '价格', name: 'price', index: 'price', width: 80 }, 			
			{ label: '卧室数量', name: 'bedroom', index: 'bedroom', width: 80 }, 			
			{ label: '厅数量', name: 'livingRoom', index: 'living_room', width: 80 }, 			
			{ label: '厕所数量', name: 'washroom', index: 'washroom', width: 80 }, 			
			{ label: '厨房数量', name: 'kitchen', index: 'kitchen', width: 80 }, 			
			{ label: '朝向', name: 'direction', index: 'direction', width: 80 }, 			
			{ label: '建筑面积', name: 'buildingAcreage', index: 'building_acreage', width: 80 }, 			
			{ label: '套内面积', name: 'insideAcreage', index: 'inside_acreage', width: 80 }, 			
			{ label: '楼层类型', name: 'flootType', index: 'floot_type', width: 80 }, 			
			{ label: '楼层数量', name: 'flootCount', index: 'floot_count', width: 80 }, 			
			{ label: '房型', name: 'roomType', index: 'room_type', width: 80 }, 			
			{ label: '装修', name: 'decoration', index: 'decoration', width: 80 }, 			
			{ label: '建筑类型', name: 'buildingType', index: 'building_type', width: 80 }, 			
			{ label: '供暖方式', name: 'heatingType', index: 'heating_type', width: 80 }, 			
			{ label: '产权', name: 'equity', index: 'equity', width: 80 }, 			
			{ label: '建筑结构', name: 'buildingStructure', index: 'building_structure', width: 80 }, 			
			{ label: '楼梯数量', name: 'stair', index: 'stair', width: 80 }, 			
			{ label: '每层住户数量', name: 'floorRoomCount', index: 'floor_room_count', width: 80 }, 			
			{ label: '是否有电梯 0：没有  1：有', name: 'elevator', index: 'elevator', width: 80 }, 			
			{ label: '创建人员', name: 'createUser', index: 'create_user', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }, 			
			{ label: '修改人员', name: 'updateUser', index: 'update_user', width: 80 }, 			
			{ label: '修改时间', name: 'updateTime', index: 'update_time', width: 80 }, 			
			{ label: '挂牌时间', name: 'saleTime', index: 'sale_time', width: 80 }, 			
			{ label: '上次交易时间', name: 'lastSaleTime', index: 'last_sale_time', width: 80 }, 			
			{ label: '抵押信息', name: 'mortgageType', index: 'mortgage_type', width: 80 }, 			
			{ label: '交易权属', name: 'ownershipType', index: 'ownership_type', width: 80 }, 			
			{ label: '房屋用途', name: 'use', index: 'use', width: 80 }, 			
			{ label: '产权归属', name: 'equityOwner', index: 'equity_owner', width: 80 }, 			
			{ label: '核心卖点', name: 'sellingPoint', index: 'selling_point', width: 80 }, 			
			{ label: '1-上架，2-下架', name: 'state', index: 'state', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		houseInfo: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.houseInfo = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.houseInfo.id == null ? "estateAgency/houseinfo/save" : "estateAgency/houseinfo/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.houseInfo),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "estateAgency/houseinfo/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "estateAgency/houseinfo/info/"+id, function(r){
                vm.houseInfo = r.houseInfo;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});