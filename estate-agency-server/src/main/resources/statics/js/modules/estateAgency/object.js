$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'estateAgency/object/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: '房屋类型 1:销售，2:出租', name: 'type', index: 'type', width: 80 }, 			
			{ label: '小区id', name: 'communityId', index: 'community_id', width: 80 }, 			
			{ label: '栋/幢/弄/胡同', name: 'houseNumberBuilding', index: 'house_number_building', width: 80 }, 			
			{ label: '单元/号', name: 'houseNumberUnit', index: 'house_number_unit', width: 80 }, 			
			{ label: '门牌', name: 'houseNumberRoom', index: 'house_number_room', width: 80 }, 			
			{ label: '楼层（当前）', name: 'currentFloor', index: 'current_floor', width: 80 }, 			
			{ label: '楼层（共）', name: 'totalFloor', index: 'total_floor', width: 80 }, 			
			{ label: '实际楼层', name: 'actualFloor', index: 'actual_floor', width: 80 }, 			
			{ label: '户型（室）', name: 'room', index: 'room', width: 80 }, 			
			{ label: '户型（厅）', name: 'hall', index: 'hall', width: 80 }, 			
			{ label: '户型（卫）', name: 'bathroom', index: 'bathroom', width: 80 }, 			
			{ label: '建筑面积', name: 'constructionArea', index: 'construction_area', width: 80 }, 			
			{ label: '套内面积', name: 'innerBuildingArea', index: 'inner_building_area', width: 80 }, 			
			{ label: '标题', name: 'smartTitle', index: 'smart_title', width: 80 }, 			
			{ label: '主卧朝向', name: 'bedroomOriented', index: 'bedroom_oriented', width: 80 }, 			
			{ label: '房屋类型', name: 'houseTypes', index: 'house_types', width: 80 }, 			
			{ label: '装修情况', name: 'decorationStatus', index: 'decoration_status', width: 80 }, 			
			{ label: '是否有电梯 0：没有  1：有', name: 'elevator', index: 'elevator', width: 80 }, 			
			{ label: '供暖', name: 'heating', index: 'heating', width: 80 }, 			
			{ label: '房源详情', name: 'houseDetail', index: 'house_detail', width: 80 }, 			
			{ label: '售价', name: 'price', index: 'price', width: 80 }, 			
			{ label: '每层住户数量', name: 'floorRoomCount', index: 'floor_room_count', width: 80 }, 			
			{ label: '创建人员', name: 'createUser', index: 'create_user', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }, 			
			{ label: '修改人员', name: 'updateUser', index: 'update_user', width: 80 }, 			
			{ label: '修改时间', name: 'updateTime', index: 'update_time', width: 80 }, 			
			{ label: '挂牌时间', name: 'saleTime', index: 'sale_time', width: 80 }, 			
			{ label: '摘牌时间', name: 'noSaleTime', index: 'no_sale_time', width: 80 }, 			
			{ label: '', name: 'homeowner', index: 'homeowner', width: 80 }, 			
			{ label: '房主电话', name: 'homeownerPhone', index: 'homeowner_phone', width: 80 }, 			
			{ label: '房主电话备用', name: 'homeownerPhone1', index: 'homeowner_phone1', width: 80 }, 			
			{ label: '房主微信', name: 'homeownerWx', index: 'homeowner_wx', width: 80 }, 			
			{ label: '1-上架，2-下架', name: 'state', index: 'state', width: 80 }, 			
			{ label: '删除标记 0:未删除 1:删除', name: 'isDelete', index: 'is_delete', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		object: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.object = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.object.id == null ? "estateAgency/object/save" : "estateAgency/object/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.object),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "estateAgency/object/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "estateAgency/object/info/"+id, function(r){
                vm.object = r.object;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});