$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'estateAgency/rent/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: 'object表外键关联', name: 'objectId', index: 'object_id', width: 80 }, 			
			{ label: '出租方式 0:合租 1:整租', name: 'rentType', index: 'rent_type', width: 80 }, 			
			{ label: '出租面积', name: 'rentArea', index: 'rent_area', width: 80 }, 			
			{ label: '磐石楼栋户室号ID', name: 'panshiHouseid', index: 'panshi_houseId', width: 80 }, 			
			{ label: '已出租数量', name: 'rentNum', index: 'rent_num', width: 80 }, 			
			{ label: '待租卧室朝向', name: 'roomOrient', index: 'room_orient', width: 80 }, 			
			{ label: '公共设施', name: 'publicFacilities', index: 'public_facilities', width: 80 }, 			
			{ label: '房间设施', name: 'relatedFacilities', index: 'related_facilities', width: 80 }, 			
			{ label: '共X户合租', name: 'totalNum', index: 'total_num', width: 80 }, 			
			{ label: '待租卧室', name: 'roomMode', index: 'room_mode', width: 80 }, 			
			{ label: '付款方式', name: 'priceMode', index: 'price_mode', width: 80 }, 			
			{ label: '最早入住时间', name: 'rentTime', index: 'rent_time', width: 80 }, 			
			{ label: '佣金', name: 'commission', index: 'commission', width: 80 }, 			
			{ label: '看房时间', name: 'visitTime', index: 'visit_time', width: 80 }, 			
			{ label: '房屋亮点', name: 'wholeHouseFeature', index: 'whole_house_feature', width: 80 }, 			
			{ label: '房屋亮点', name: 'shareHouseFeature', index: 'share_house_feature', width: 80 }, 			
			{ label: '费用明细', name: 'feeDetail', index: 'fee_detail', width: 80 }, 			
			{ label: '出租要求', name: 'rentDemand', index: 'rent_demand', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		rent: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.rent = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.rent.id == null ? "estateAgency/rent/save" : "estateAgency/rent/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.rent),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "estateAgency/rent/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "estateAgency/rent/info/"+id, function(r){
                vm.rent = r.rent;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});