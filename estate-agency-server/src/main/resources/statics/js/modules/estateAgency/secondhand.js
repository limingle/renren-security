$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'estateAgency/secondhand/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: 'object表外键关联', name: 'objectId', index: 'object_id', width: 80 }, 			
			{ label: '服务介绍', name: 'serviceIntro', index: 'service_intro', width: 80 }, 			
			{ label: '业主心态', name: 'ownerFeel', index: 'owner_feel', width: 80 }, 			
			{ label: '建造年代', name: 'constructionAge', index: 'construction_age', width: 80 }, 			
			{ label: '建筑形式', name: 'buildingTypes', index: 'building_types', width: 80 }, 			
			{ label: '售卖类型 0: 二手房 1: 新房', name: 'saleType', index: 'sale_type', width: 80 }, 			
			{ label: '产权年限', name: 'propertyYears', index: 'property_years', width: 80 }, 			
			{ label: '是否为业主唯一住房', name: 'uniqueHouse', index: 'unique_house', width: 80 }, 			
			{ label: '房屋年限 0：否 1：是', name: 'houseYearLimit', index: 'house_year_limit', width: 80 }, 			
			{ label: '首付', name: 'payinAdvance', index: 'payIn_advance', width: 80 }, 			
			{ label: '佣金比例≤', name: 'commission', index: 'commission', width: 80 }, 			
			{ label: '备案编号', name: 'recordNumber', index: 'record_number', width: 80 }, 			
			{ label: '产权类型', name: 'propertyTypes', index: 'property_types', width: 80 }, 			
			{ label: '磐石楼栋户室号ID', name: 'panshiHouseId', index: 'panshi_house_id', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		secondHand: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.secondHand = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.secondHand.id == null ? "estateAgency/secondhand/save" : "estateAgency/secondhand/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.secondHand),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "estateAgency/secondhand/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "estateAgency/secondhand/info/"+id, function(r){
                vm.secondHand = r.secondHand;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});