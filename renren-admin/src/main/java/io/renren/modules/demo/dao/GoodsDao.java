package io.renren.modules.demo.dao;

import io.renren.modules.demo.entity.GoodsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品管理
 * 
 * @author Mark
 * @email limingle@outlook.com
 * @date 2020-03-18 10:54:00
 */
@Mapper
public interface GoodsDao extends BaseMapper<GoodsEntity> {
	
}
