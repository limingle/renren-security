/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.sys.dto.SysDictDTO;
import io.renren.modules.sys.dto.SysDictDetailDTO;
import io.renren.modules.sys.entity.SysDictEntity;
import io.renren.modules.sys.service.SysDictService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.beans.Transient;
import java.util.*;

/**
 * 数据字典
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("sys/dict")
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:dict:list")
    public R list(@RequestParam Map<String, Object> params) {
        List<SysDictDTO> sysDictDtos = null;
        PageHelper.startPage(Integer.parseInt(params.get("page").toString()) , Integer.parseInt(params.get("limit").toString()));
        sysDictDtos= sysDictService.queryList(params);

        return R.ok().put("page", new PageInfo<>(sysDictDtos));
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{type}")
    @RequiresPermissions("sys:dict:info")
    public R info(@PathVariable("type") String type) {
        List<SysDictEntity> dictList = sysDictService.getDicListByType(type);
        if (dictList.size() == 0) {
            return R.error();
        }
        SysDictEntity dict = dictList.get(0);
        Map<String, Object> result = new HashMap<>();
        result.put("name",dict.getName());
        result.put("type",dict.getType());
        result.put("remark",dict.getRemark());
        List<SysDictDetailDTO> sysDictDetailDTOs = new ArrayList<>();
        for (SysDictEntity sde : dictList) {
            SysDictDetailDTO sdde = new SysDictDetailDTO();
            sdde.setCode(sde.getCode());
            sdde.setDelFlag(sde.getDelFlag());
            sdde.setOrderNum(sde.getOrderNum());
            sdde.setValue(sde.getValue());
            sysDictDetailDTOs.add(sdde);
        }
        result.put("dictDetailList",sysDictDetailDTOs);

        return R.ok().put("data", result);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:dict:save")
    @Transactional
    public R save(@RequestBody SysDictDTO sysDictDTO) {
        //校验类型
        ValidatorUtils.validateEntity(sysDictDTO);
        for (SysDictDetailDTO sysDictDetailDTO : sysDictDTO.getDictDetailList()) {
            ValidatorUtils.validateEntity(sysDictDetailDTO);
        }

        forSaveDict(sysDictDTO);

        return R.ok();
    }

    private void forSaveDict(@RequestBody SysDictDTO sysDictDTO) {
        for (SysDictDetailDTO sysDictDetailDTO : sysDictDTO.getDictDetailList()) {
            SysDictEntity sysDictEntity = new SysDictEntity();
            sysDictEntity.setName(sysDictDTO.getName());
            sysDictEntity.setRemark(sysDictDTO.getRemark());
            sysDictEntity.setType(sysDictDTO.getType());
            sysDictEntity.setCode(sysDictDetailDTO.getCode());
            sysDictEntity.setValue(sysDictDetailDTO.getValue());
            sysDictEntity.setDelFlag(sysDictDetailDTO.getDelFlag());
            sysDictEntity.setOrderNum(sysDictDetailDTO.getOrderNum());
            sysDictService.save(sysDictEntity);
        }
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:dict:update")
    @Transactional
    public R update(@RequestBody SysDictDTO sysDictDTO) {
        //校验类型
        ValidatorUtils.validateEntity(sysDictDTO);
        for (SysDictDetailDTO sysDictDetailDTO : sysDictDTO.getDictDetailList()) {
            ValidatorUtils.validateEntity(sysDictDetailDTO);
        }

        List<String> types=new ArrayList<>();
        types.add(sysDictDTO.getType());
        Integer result=sysDictService.deleteDictByType(types);

        if(result==0){
            return  R.error();
        }

        forSaveDict(sysDictDTO);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:dict:delete")
    public R delete(@RequestBody List<String> types) {
        sysDictService.deleteDictByType(types);

        return R.ok();
    }

}
