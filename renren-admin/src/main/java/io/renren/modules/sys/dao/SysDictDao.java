/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.dto.SysDictDTO;
import io.renren.modules.sys.entity.SysDictEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 数据字典
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SysDictDao extends BaseMapper<SysDictEntity> {
    /**
     * 查询列表
     * @param {name} 类型
     */
    List<SysDictDTO> queryList(String name);

    /**
     * 根据类型查询字典
     * @param {type} 类型
     */
    List<SysDictEntity> queryListByType(String type);

    /***
     * 根据类型删除字典
     * @param types
     * @return
     */
    Integer deleteDictByType(Map<String, Object> types);

}
