/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.SysDictDao;
import io.renren.modules.sys.dto.SysDictDTO;
import io.renren.modules.sys.entity.SysDictEntity;
import io.renren.modules.sys.service.SysDictService;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.convert.EntityWriter;
import org.springframework.stereotype.Service;

import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("sysDictService")
public class SysDictServiceImpl extends ServiceImpl<SysDictDao, SysDictEntity> implements SysDictService {

    @Override
    public List<SysDictDTO> queryList(Map<String, Object> params) {
        String name = (String) params.get("name");

        return  baseMapper.queryList(name);

//        IPage<SysDictEntity> page = this.page(
//                new Query<SysDictEntity>().getPage(params),
//                new QueryWrapper<SysDictEntity>()
//                        .select("DISTINCT type,`name`,remark")
//                        .like(StringUtils.isNotBlank(name), "name", name)
//        );
//
//        return new PageUtils(page);
    }

    @Override
    public List<SysDictEntity> getDicListByType(String type) {
        List<SysDictEntity> dictEntityList=baseMapper.queryListByType(type);
        return dictEntityList;
    }

    @Override
    public Integer deleteDictByType(List<String> types) {
//        baseMapper.
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("types", types);
        return baseMapper.deleteDictByType(map);
    }

}
